﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHang.BUS.ChucNang;
using QuanLyBanHang.Model.ChucNang;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraEditors;

namespace QLBanHang
{
    public partial class PhieuNhapHangControl : UserControl
    {
        PhieuMuaHangBus busPhieuMuaHang = new PhieuMuaHangBus();
        ChiTietMuaHangBus busChiTiet = new ChiTietMuaHangBus();
        HangHoaBus HangBus = new HangHoaBus();
        DataTable dt = new DataTable();
        BindingList<ChiTietPhieuNhapHang> data;
        public PhieuNhapHangControl()
        {
            InitializeComponent();
            Load += PhieuNhapHangControl_Load;
        }

        private void LoadNhaCungCap()
        {
            NhaCungCapBus bus = new NhaCungCapBus();
            List<NhaCungCapModel> list = bus.DsNhaCungCap();
            lkNhaCungCap.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkNhaCungCap.Properties.DataSource = list;
            lkNhaCungCap.Properties.DisplayMember = "TenNhaCungCap";
            lkNhaCungCap.Properties.ValueMember = "MaNhaCungCap";

            GridColumn col1 = lkNhaCungCap.Properties.View.Columns.AddField("TenNhaCungCap");
            col1.VisibleIndex = 0;
            col1.Caption = "Nhà Phân Phỗi";

            GridColumn col2 = lkNhaCungCap.Properties.View.Columns.AddField("MaNhaCungCap");
            col2.VisibleIndex = 1;
            col2.Caption = "Mã";

            lkMaNhaCungCap.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkMaNhaCungCap.Properties.DataSource = list;
            lkMaNhaCungCap.Properties.DisplayMember = "MaNhaCungCap";
            lkMaNhaCungCap.Properties.ValueMember = "MaNhaCungCap";

            GridColumn col3 = lkMaNhaCungCap.Properties.View.Columns.AddField("MaNhaCungCap");
            col3.VisibleIndex = 0;
            col3.Caption = "Nhà Phân Phỗi";

            GridColumn col4 = lkMaNhaCungCap.Properties.View.Columns.AddField("TenNhaCungCap");
            col4.VisibleIndex = 1;
            col4.Caption = "Mã";
            lkMaNhaCungCap.Text = string.Empty;
            lkNhaCungCap.Text = string.Empty;
        }
        private void PhieuMuaHangMoi()
        {
            txtPhieu1.Text = busPhieuMuaHang.MaPhieuMuaHangMoi();
            txtPhieu1.ReadOnly = true;
            dtNgay.Text = DateTime.Today.ToShortDateString().ToString();           
            dtHTT.Text = DateTime.Today.ToShortDateString().ToString();
        }
        private void LoadNhanVien()
        {
            NhanVienBus bus = new NhanVienBus();
            List<Model.NhanVienModel> list = bus.DsNhanVien();
            lkNhanVien.Properties.DataSource = list;  
        }
        private void LoadKhoNhap()
        {
            lkKhoHang.Text = string.Empty;
            KhoHangBus bus = new KhoHangBus();
            List<Model.KhoHangModel> list = bus.DsKhoHang();
            lkKhoHang.Properties.DataSource = list;
        }
        private void LoadDKTT()
        {
            cbDKTT.Items.Add("Công nợ");
            cbDKTT.Items.Add("Thanh toán ngay");
        }
        private void LoadHTTT()
        {
            cbHTTT.Items.Add("Tiền mặt");
            cbHTTT.Items.Add("Chuyển khoản");
        }
        private void PhieuNhapHangControl_Load(object sender, EventArgs e)
        {
            SetDataSource_DisplayMember_ValueMember();
            LoadNhaCungCap();
            PhieuMuaHangMoi();
            LoadNhanVien();
            LoadKhoNhap();
            LoadDKTT();
            LoadHTTT();
            LoadHangHoa();
            data = new BindingList<ChiTietPhieuNhapHang>();
            dgvNhapHang.DataSource = data;
        }
        private void TimNhaCungCap(string MaNCC)
        {
            NhaCungCapBus bus = new NhaCungCapBus();
            NhaCungCapModel kh = bus.TimTheoMaNhaCungCap(MaNCC.Trim());
            txtDiaChi.Text = kh.DiaChi;
            txtDienThoai.Text = kh.DienThoai;
        }
        private void lkNhaCungCap_EditValueChanged(object sender, EventArgs e)
        {
            TimNhaCungCap(lkNhaCungCap.EditValue.ToString());
            lkMaNhaCungCap.EditValue = lkNhaCungCap.EditValue.ToString();
        }

        private void lkMaNhaCungCap_EditValueChanged(object sender, EventArgs e)
        {
            TimNhaCungCap(lkNhaCungCap.EditValue.ToString());
            lkNhaCungCap.EditValue = lkMaNhaCungCap.Text;
        }
        private void SetDataSource_DisplayMember_ValueMember()
        {
            /*Cấu hình Lookup Kho Hàng*/

            lkKhoHang.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkKhoHang.Properties.DisplayMember = "TenKho";
            lkKhoHang.Properties.ValueMember = "MaKho";
            /*Cấu hình Lookup Tên Hàng*/
            lkTenHangHoa.DisplayMember = "TenHangHoa";
            lkTenHangHoa.ValueMember = "TenHangHoa";
            lkTenHangHoa.PopupWidth = 1000;
            /*Cấu hình Lookup Mã Hàng*/
            lkMaHangHoa.DisplayMember = "MaHangHoa";
            lkMaHangHoa.ValueMember = "MaHangHoa";
            lkMaHangHoa.PopupWidth = 1000;
            
            
            /*Cấu hình Lookup Nhân Viên*/
            lkNhanVien.Properties.View.OptionsBehavior.AutoPopulateColumns = false;

            lkNhanVien.Properties.DisplayMember = "HoTen";
            lkNhanVien.Properties.ValueMember = "MaNhanVien";

            GridColumn nv = lkNhanVien.Properties.View.Columns.AddField("MaNhanVien");
            nv.VisibleIndex = 0;
            nv.Caption = "Mã";

            GridColumn nv2 = lkNhanVien.Properties.View.Columns.AddField("HoTen");
            nv2.VisibleIndex = 1;
            nv2.Caption = "Tên";

            lkNhanVien.Properties.View.BestFitColumns();
            lkNhanVien.Properties.PopupFormWidth = 300;
            /*Cấu hình Lookup Kho*/
            lkKhoHang.Text = string.Empty;
            GridColumn kho = lkKhoHang.Properties.View.Columns.AddField("MaKho");
            kho.VisibleIndex = 0;
            kho.Caption = "Mã";

            GridColumn kho2 = lkKhoHang.Properties.View.Columns.AddField("TenKho");
            kho2.VisibleIndex = 1;
            kho2.Caption = "Tên";

            lkKhoHang.Properties.View.BestFitColumns();
            lkKhoHang.Properties.PopupFormWidth = 300;
        }
        private void LoadHangHoa()
        {
            List<QuanLyBanHang.Model.ChucNang.HangHoaModel> list = HangBus.DsHangHoa();
            lkTenHangHoa.DataSource = list;
            lkMaHangHoa.DataSource = list;
        }
        private void lkMaHangHoa_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit lookUp = sender as LookUpEdit;
            HangHoaModel hh = HangBus.TimTheoMaHH(lookUp.EditValue.ToString());
            gridViewNhapHang.SetFocusedRowCellValue("MaHangHoa", hh.MaHangHoa);
            gridViewNhapHang.SetFocusedRowCellValue("TenHangHoa", hh.TenHangHoa);
            gridViewNhapHang.SetFocusedRowCellValue("TenDonVi", hh.TenDonVi);
            gridViewNhapHang.SetFocusedRowCellValue("SoLuong", 1);
            gridViewNhapHang.SetFocusedRowCellValue("DonGia", hh.GiaBanLe);
            int sl = int.Parse(gridViewNhapHang.GetFocusedRowCellValue("SoLuong").ToString());
            gridViewNhapHang.SetFocusedRowCellValue("ThanhTien", sl * hh.GiaBanLe);
        }

        private void lkTenHangHoa_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit TenHangHoa = sender as LookUpEdit;
            string Ma = HangBus.TimMa(TenHangHoa.Text);
            HangHoaModel hh = HangBus.TimTheoMaHH(Ma);
            gridViewNhapHang.SetFocusedRowCellValue("MaHangHoa", hh.MaHangHoa);
            gridViewNhapHang.SetFocusedRowCellValue("TenHangHoa", hh.TenHangHoa);
            gridViewNhapHang.SetFocusedRowCellValue("DonVi", hh.TenDonVi);
            gridViewNhapHang.SetFocusedRowCellValue("SoLuong", 1);
            gridViewNhapHang.SetFocusedRowCellValue("DonGia", hh.GiaBanLe);
            int sl = int.Parse(gridViewNhapHang.GetFocusedRowCellValue("SoLuong").ToString());
            gridViewNhapHang.SetFocusedRowCellValue("ThanhTien", sl * hh.GiaBanLe);
        }
        private PhieuMuaHang LayThongTinPhieuNhapHang()
        {
            PhieuMuaHang pb = new PhieuMuaHang();
            pb.MaPhieuNhapHang = busPhieuMuaHang.MaPhieuMuaHangMoi();
            pb.MaNhaCungCap = lkMaNhaCungCap.EditValue.ToString();
            pb.DiaChi = txtDiaChi.Text;
            pb.DienThoai = txtDienThoai.Text;
            pb.NgayNhap = DateTime.Parse(dtNgay.Text);
            pb.GhiChu = txtGhiChu.Text;
            pb.NhanVien = lkNhanVien.EditValue.ToString();
            pb.KhoNhap = lkKhoHang.EditValue.ToString();
            pb.DieuKhoanThanhToan = cbDKTT.Text;
            pb.HinhThucThanhToan = cbHTTT.Text;
            pb.HanThanhToan = DateTime.Parse(dtHTT.Text);
            return pb;
        }
        private bool KiemTra()
        {
            if (lkNhaCungCap.Text == string.Empty)
            {
                MessageBox.Show("Chưa chọn nhà cung cấp");
                return false;
            }
            if (lkNhanVien.Text == string.Empty)
            {
                MessageBox.Show("Chưa chọn nhân viên");
                return false;
            } 
            if (lkKhoHang.Text == string.Empty)
            {
                MessageBox.Show("Chưa chọn kho");
                return false;
            }
            return true;
        }
        private void LoadDL()
        {
            txtDiaChi.Text = string.Empty;
            txtDienThoai.Text = string.Empty;
            txtGhiChu.Text = string.Empty;
            txtPhieu1.Text = string.Empty;
            txtSoHoaDonVat.Text = string.Empty;
            txtSoPhieuVietTay.Text = string.Empty;
            lkKhoHang.Text = string.Empty;
            lkNhanVien.Text = string.Empty;
            lkNhaCungCap.Text = string.Empty;
            lkMaNhaCungCap.Text = string.Empty;
            data = new BindingList<ChiTietPhieuNhapHang>();
            dgvNhapHang.DataSource = data;
            PhieuMuaHangMoi();
        }
        private void btnLuuVaThem_Click(object sender, EventArgs e)
        {
            if (KiemTra() == true)
            {
                // thuc hien them phieu ban mua moi
                PhieuMuaHang pb = LayThongTinPhieuNhapHang();
                busPhieuMuaHang.ThemPhieuNhapHang(pb);
                //them chi tiet phieu mua hang
                for (int i = 0; i < data.Count(); i++)
                {
                    ChiTietPhieuNhapHang ct = new ChiTietPhieuNhapHang();
                    ct.MaPhieuNhapHang = txtPhieu1.Text;
                    ct.MaChiTietPhieuNhapHang = busPhieuMuaHang.MaChiTietMoi();
                    ct.MaHangHoa = data[i].MaHangHoa;
                    ct.SoLuong = data[i].SoLuong;
                    ct.DonGia = data[i].DonGia;
                    ct.ThanhTien = data[i].ThanhTien;
                    busChiTiet.ThemChiTiet(ct);
                }
                MessageBox.Show("Thêm thành công");
                LoadDL();
            }
        }

        private void iSoLuong_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit soLuong = sender as SpinEdit;
            if (Int32.Parse(soLuong.EditValue.ToString()) < 0)
            {
                soLuong.EditValue = 1;
            }
            decimal donGia = decimal.Parse(gridViewNhapHang.GetRowCellValue(gridViewNhapHang.FocusedRowHandle, "DonGia").ToString());
            gridViewNhapHang.SetFocusedRowCellValue("ThanhTien", Int32.Parse(soLuong.EditValue.ToString()) * donGia);
        }
    }
}
