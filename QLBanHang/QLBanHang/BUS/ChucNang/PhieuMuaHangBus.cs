﻿using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class PhieuMuaHangBus
    {
        PhieuMuaHangDao dao = new PhieuMuaHangDao();
        public string MaPhieuMuaHangMoi()
        {
            var list = dao.DsPhieuBanHang();
            if(list == null)
            {
                return "MH001";
            }
            List<string> ds = new List<string>();
            foreach (var item in list)
            {
                ds.Add(item.MaPhieuNhapHang);
            }
            List<int> ds_id = new List<int>();
            foreach (var rs in ds)
            {
                rs.Trim();
                ds_id.Add(Int32.Parse(rs.Substring(2)));
            }
            ds_id.Sort();
            int id = ds_id.Count();
            int i;
            for (i = 0; i < ds_id.Count(); i++)
            {
                if ((i + 1) != ds_id[i])
                {

                    break;
                }
            }
            id = i + 1;
            string s = "";
            if (id < 10)
            {
                s = "MH00" + id.ToString();
            }
            else if (id < 100)
            {
                s = "MH0" + id.ToString();
            }
            else
            {
                s = "MH" + id.ToString();
            }
            return s;
        }
        public void ThemPhieuNhapHang(PhieuMuaHang pb)
        {
            dao.ThemPhieuMuaHang(pb);
        }

        internal string MaChiTietMoi()
        {
            var list = dao.DsChiTiet();
            List<string> ds = new List<string>();
            foreach (var item in list)
            {
                ds.Add(item.MaChiTietPhieuNhapHang);
            }
            List<int> ds_id = new List<int>();
            foreach (var rs in ds)
            {
                rs.Trim();
                ds_id.Add(Int32.Parse(rs.Substring(2)));
            }
            ds_id.Sort();
            int id = ds_id.Count();
            int i;
            for (i = 0; i < ds_id.Count(); i++)
            {
                if ((i + 1) != ds_id[i])
                {

                    break;
                }
            }
            id = i + 1;
            string s = "";
            if (id < 10)
            {
                s = "CT00" + id.ToString();
            }
            else if (id < 100)
            {
                s = "CT0" + id.ToString();
            }
            else
            {
                s = "CT" + id.ToString();
            }
            return s;
        }
    }
}
