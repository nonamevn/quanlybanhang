﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QLBanHang.Model;
using DevExpress.XtraGrid.Columns;
using QuanLyBanHang.Model.ChucNang;
using QuanLyBanHang.BUS.ChucNang;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;

namespace QLBanHang
{
    public partial class PhieuBanHangControl : UserControl
    {
        KhachHangBus bus = new KhachHangBus();
        HangHoaBus HangBus = new HangHoaBus();
        PhieuBanHangBus PhieuBanHangBus = new PhieuBanHangBus();
        ChiTietPhieuBanHangBus ChiTietBus = new ChiTietPhieuBanHangBus();
        DataTable dt = new DataTable();
        BindingList<ChiTietPhieuBanHang> data;
        public PhieuBanHangControl()
        {
            InitializeComponent();
            Load += PhieuBanHangControl_Load;
        }
        private void LoadKhachHang()
        {
            
            List<KhachHangModel> list = bus.DanhSachKH();
            cbMaKhachHang.Properties.DataSource = list;
            cbbKhachHang.Properties.DataSource = list;
        }
        private void PhieuBanHangMoi()
        {
            txtPhieu1.Text = PhieuBanHangBus.MaPhieuBanHangMoi();
            txtPhieu1.ReadOnly = true;
            dtNgay.Text = DateTime.Today.ToShortDateString().ToString();
            dtHanThanhToan.Text = DateTime.Today.ToShortDateString().ToString();
            dtNgayGiao.Text = DateTime.Today.ToShortDateString().ToString();
        }
        private void LoadDL()
        {
            txtDiaChi.Text = string.Empty;
            txtDienThoai.Text = string.Empty;
            txtGhiChu.Text = string.Empty;
            txtPhieu1.Text = string.Empty;
            txtSoHoaDonVat.Text = string.Empty;
            txtSoPhieuVietTay.Text = string.Empty;
            lkKhoHang.Text = string.Empty;
            lkNhanVien.Text = string.Empty;
            cbbKhachHang.Text = string.Empty;
            cbMaKhachHang.Text = string.Empty;
            data = new BindingList<ChiTietPhieuBanHang>();
            dgvBanHang.DataSource = data;
            PhieuBanHangMoi();
        }
        private void LoadNhanVien()
        {
            NhanVienBus bus = new NhanVienBus();
            List<NhanVienModel> list = bus.DsNhanVien();
            lkNhanVien.Properties.DataSource = list;
            
        }
        private void LoadKhoXuat()
        {
           
            KhoHangBus bus = new KhoHangBus();
            List<KhoHangModel> list = bus.DsKhoHang();
            lkKhoHang.Properties.DataSource = list;
        }
        private void LoadHinhThuc()
        {
            cbHTTT.Items.Add("Tiền mặt");
            cbHTTT.Items.Add("Chuyển khoản");
        }
        private void LoadDieuKhoanThanhToan()
        {
            cbDKTT.Items.Add("Công nợ");
            cbDKTT.Items.Add("Thanh toán ngay");
        }
        private void PhieuBanHangControl_Load(object sender, EventArgs e)
        {
            SetDataSource_DisplayMember_ValueMember();
            LoadKhachHang();
            LoadHangHoa();
            PhieuBanHangMoi();
            LoadNhanVien();
            LoadKhoXuat();
            LoadHinhThuc();
            LoadDieuKhoanThanhToan();
            data = new BindingList<ChiTietPhieuBanHang>();
            dgvBanHang.DataSource = data;
        }
        private void TimKhachHang(string MaKH)
        {
            KhachHangModel kh = bus.TimTheoMaKH(MaKH.Trim());
            txtDiaChi.Text = kh.DiaChi;
            txtDienThoai.Text = kh.DienThoai;
        }
        private void cbMaKhachHang_TextChanged(object sender, EventArgs e)
        {
            TimKhachHang(cbMaKhachHang.Text);
            cbbKhachHang.EditValue = cbMaKhachHang.Text;
        }

        private void cbbKhachHang_TextChanged(object sender, EventArgs e)
        {
            TimKhachHang(cbbKhachHang.EditValue.ToString());
            cbMaKhachHang.EditValue = cbbKhachHang.EditValue.ToString();
        }

        private void SetDataSource_DisplayMember_ValueMember()
        {
            /*Cấu hình Lookup Kho Hàng*/

            lkKhoHang.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkKhoHang.Properties.DisplayMember = "TenKho";
            lkKhoHang.Properties.ValueMember = "MaKho";
            /*Cấu hình Lookup Tên Hàng*/
            lkTenHangHoa.DisplayMember = "TenHangHoa";
            lkTenHangHoa.ValueMember = "TenHangHoa";
            lkTenHangHoa.PopupWidth = 1000;
            /*Cấu hình Lookup Mã Hàng*/
            lkMaHangHoa.DisplayMember = "MaHangHoa";
            lkMaHangHoa.ValueMember = "MaHangHoa";
            lkMaHangHoa.PopupWidth = 1000;
            /*Cấu hình Lookup Khách Hàng*/
            cbMaKhachHang.Text = string.Empty;
            cbbKhachHang.Text = string.Empty;
            cbMaKhachHang.Properties.View.OptionsBehavior.AutoPopulateColumns = false;

            cbMaKhachHang.Properties.DisplayMember = "MaKhachHang";
            cbMaKhachHang.Properties.ValueMember = "MaKhachHang";
            GridColumn col1 = cbMaKhachHang.Properties.View.Columns.AddField("MaKhachHang");
            col1.VisibleIndex = 0;
            col1.Caption = "Mã";
            GridColumn col2 = cbMaKhachHang.Properties.View.Columns.AddField("TenKhachHang");
            col2.VisibleIndex = 1;
            col2.Caption = "Tên";
            cbMaKhachHang.Properties.View.BestFitColumns();
            cbMaKhachHang.Properties.PopupFormWidth = 300;
            // Tên Khách Hàng
            cbbKhachHang.Properties.View.OptionsBehavior.AutoPopulateColumns = false;

            cbbKhachHang.Properties.DisplayMember = "TenKhachHang";
            cbbKhachHang.Properties.ValueMember = "MaKhachHang";
            GridColumn col3 = cbbKhachHang.Properties.View.Columns.AddField("TenKhachHang");
            col3.VisibleIndex = 0;
            col3.Caption = "Khách Hàng";
            GridColumn col4 = cbbKhachHang.Properties.View.Columns.AddField("MaKhachHang");
            col4.VisibleIndex = 1;
            col4.Caption = "Mã";
            cbbKhachHang.Properties.View.BestFitColumns();
            cbbKhachHang.Properties.PopupFormWidth = 300;
            /*Cấu hình Lookup Nhân Viên*/
            lkNhanVien.Properties.View.OptionsBehavior.AutoPopulateColumns = false;

            lkNhanVien.Properties.DisplayMember = "HoTen";
            lkNhanVien.Properties.ValueMember = "MaNhanVien";

            GridColumn nv = lkNhanVien.Properties.View.Columns.AddField("MaNhanVien");
            nv.VisibleIndex = 0;
            nv.Caption = "Mã";

            GridColumn nv2 = lkNhanVien.Properties.View.Columns.AddField("HoTen");
            nv2.VisibleIndex = 1;
            nv2.Caption = "Tên";

            lkNhanVien.Properties.View.BestFitColumns();
            lkNhanVien.Properties.PopupFormWidth = 300;
            /*Cấu hình Lookup Kho*/
            lkKhoHang.Text = string.Empty;
            GridColumn kho = lkKhoHang.Properties.View.Columns.AddField("MaKho");
            kho.VisibleIndex = 0;
            kho.Caption = "Mã";

            GridColumn kho2 = lkKhoHang.Properties.View.Columns.AddField("TenKho");
            kho2.VisibleIndex = 1;
            kho2.Caption = "Tên";

            lkKhoHang.Properties.View.BestFitColumns();
            lkKhoHang.Properties.PopupFormWidth = 300;
        }

        private void LoadHangHoa()
        {
            List<QuanLyBanHang.Model.ChucNang.HangHoaModel> list = HangBus.DsHangHoa();
            lkTenHangHoa.DataSource = list;
            lkMaHangHoa.DataSource = list;
        }

        private void lkMaHangHoa_EditValueChanged(object sender, EventArgs e)
        {
            lkTenHangHoa.ValueMember = "TenHangHoa";
            LookUpEdit lookUp = sender as LookUpEdit;
            QuanLyBanHang.Model.ChucNang.HangHoaModel hh = HangBus.TimTheoMaHH(lookUp.EditValue.ToString());
            gridViewBanHang.SetFocusedRowCellValue("MaHangHoa", hh.MaHangHoa);
            gridViewBanHang.SetFocusedRowCellValue("TenHangHoa", hh.TenHangHoa);
            gridViewBanHang.SetFocusedRowCellValue("DonVi", hh.TenDonVi);
            gridViewBanHang.SetFocusedRowCellValue("SoLuong", 1);
            gridViewBanHang.SetFocusedRowCellValue("DonGia", hh.GiaBanLe);
            int sl = int.Parse(gridViewBanHang.GetFocusedRowCellValue("SoLuong").ToString());
            gridViewBanHang.SetFocusedRowCellValue("ThanhTien", sl * hh.GiaBanLe);
            gridViewBanHang.SetFocusedRowCellValue("ChietKhau", 0);
            gridViewBanHang.SetFocusedRowCellValue("_ChietKhau", 0);
            gridViewBanHang.SetFocusedRowCellValue("ThanhToan", 0);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {

        }
        private decimal TongTien()
        {
            decimal Tong = 0;
            for (int i = 0; i < data.Count(); i++)
            {
                Tong += data[i].ThanhToan;
            }
            return Tong;
        }
        private PhieuBanHang LayThongTinPhieuBanHang()
        {
            PhieuBanHang pb = new PhieuBanHang();
            pb.MaPhieuBanHang = PhieuBanHangBus.MaPhieuBanHangMoi();
            pb.MaKhachHang = cbMaKhachHang.EditValue.ToString();
            pb.DiaChi = txtDiaChi.Text;
            pb.DienThoai = txtDienThoai.Text;
            pb.NgayBan = DateTime.Parse(dtNgay.Text);
            pb.GhiChu = txtGhiChu.Text;
            pb.NhanVien = lkNhanVien.EditValue.ToString();
            pb.KhoXuat = lkKhoHang.EditValue.ToString();
            pb.DieuKhoanThanhToan = cbDKTT.Text;
            pb.HinhThucThanhToan = cbHTTT.Text;
            pb.ThanhTien = TongTien();
            pb.HanThanhToan = DateTime.Parse(dtHanThanhToan.Text);
            pb.NgayGiao = DateTime.Parse(dtNgayGiao.Text);
            return pb;
        }
        private bool KiemTra()
        {
            if (cbMaKhachHang.Text == string.Empty)
            {
                MessageBox.Show("Chưa chọn khách hàng");
                return false;
            }
            if (lkNhanVien.Text == string.Empty)
            {
                MessageBox.Show("Chưa chọn nhân viên");
                return false;
            }
            if (lkKhoHang.Text == string.Empty)
            {
                MessageBox.Show("Chưa chọn kho");
                return false;
            }
            return true;
        }
        private void btnLuuVaThem_Click(object sender, EventArgs e)
        {
            if (KiemTra() == true)
            {
                // thuc hien them phieu ban hang moi
                PhieuBanHang pb = LayThongTinPhieuBanHang();
                PhieuBanHangBus.ThemPhieuBanHang(pb);
                //them chi tiet phieu ban hang
                for (int i = 0; i < data.Count(); i++)
                {
                    ChiTietPhieuBanHang ct = new ChiTietPhieuBanHang();
                    ct.MaPhieuBanHang = txtPhieu1.Text;
                    ct.MaChiTietPhieuBanHang = PhieuBanHangBus.MaChiTietMoi();
                    ct.MaHangHoa = data[i].MaHangHoa;
                    ct.SoLuong = data[i].SoLuong;
                    ct.DonGia = data[i].DonGia;
                    ct.ThanhTien = data[i].ThanhTien;
                    ct.ChietKhau = data[i].ChietKhau;
                    ct._ChietKhau = data[i]._ChietKhau;
                    ct.ThanhToan = data[i].ThanhToan;
                    ChiTietBus.ThemChiTiet(ct);
                }
                MessageBox.Show("Thêm thành công");
                LoadDL();
            }
        }

        private void btnLuuVaDong_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(data[0].MaHangHoa);
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            gridViewBanHang.ShowPrintPreview();
        }

        private void lkTenHangHoa_EditValueChanged(object sender, EventArgs e)
        {
            LookUpEdit TenHangHoa = sender as LookUpEdit;
            string Ma = HangBus.TimMa(TenHangHoa.Text);
            QuanLyBanHang.Model.ChucNang.HangHoaModel hh = HangBus.TimTheoMaHH(Ma);
            gridViewBanHang.SetFocusedRowCellValue("MaHangHoa", hh.MaHangHoa);
            gridViewBanHang.SetFocusedRowCellValue("TenHangHoa", hh.TenHangHoa);
            gridViewBanHang.SetFocusedRowCellValue("DonVi", hh.TenDonVi);
            gridViewBanHang.SetFocusedRowCellValue("SoLuong", 1);
            gridViewBanHang.SetFocusedRowCellValue("DonGia", hh.GiaBanLe);
            int sl = int.Parse(gridViewBanHang.GetFocusedRowCellValue("SoLuong").ToString());
            gridViewBanHang.SetFocusedRowCellValue("ThanhTien", sl * hh.GiaBanLe);
            gridViewBanHang.SetFocusedRowCellValue("ChietKhau", 0);
            gridViewBanHang.SetFocusedRowCellValue("_ChietKhau", 0);
            gridViewBanHang.SetFocusedRowCellValue("ThanhToan", 0);

        }

        private void iSoLuong_EditValueChanged(object sender, EventArgs e)
        {
            SpinEdit soLuong = sender as SpinEdit;
            if (Int32.Parse(soLuong.EditValue.ToString()) < 0)
            {
                soLuong.EditValue = 1;
            }
            decimal donGia = decimal.Parse(gridViewBanHang.GetRowCellValue(gridViewBanHang.FocusedRowHandle, "DonGia").ToString());
            gridViewBanHang.SetFocusedRowCellValue("ThanhTien", Int32.Parse(soLuong.EditValue.ToString()) * donGia);
        }

        private void calCK_EditValueChanged(object sender, EventArgs e)
        {
            CalcEdit ck = sender as CalcEdit;
            if (Int32.Parse(ck.EditValue.ToString()) > 100)
            {
                ck.EditValue = 100;
            }
            if (Int32.Parse(ck.EditValue.ToString()) < 0)
            {
                ck.EditValue = 0;
            }
            decimal thanhTien = decimal.Parse(gridViewBanHang.GetRowCellValue(gridViewBanHang.FocusedRowHandle, "ThanhTien").ToString());
            decimal _chietKhau = (thanhTien / 100) * (Int32.Parse(ck.EditValue.ToString()));
            gridViewBanHang.SetFocusedRowCellValue("_ChietKhau", _chietKhau);
            gridViewBanHang.SetFocusedRowCellValue("ThanhToan", thanhTien - _chietKhau);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            frmKhachHang themKH = new frmKhachHang();
            themKH.GetKhachHang = (string maKH) =>
            {
                //Update lại datasource khi người dùng mới được thêm
                LoadKhachHang();
                //Truy vấn người dùng mới thêm
                TimKhachHang(maKH);
                cbbKhachHang.EditValue = maKH;
            };

            themKH.ShowDialog();

        }
    }
}
