﻿using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class NhaCungCapDao
    {
        public List<NhaCungCapModel> DsNhaCungCap()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.NHACUNGCAP.Select(k => new NhaCungCapModel
                {
                    MaNhaCungCap = k.MaNhaCungCap,
                    TenNhaCungCap = k.TenNhaCungCap
                })
                .ToList();
                return list;
            }
        }

        public NhaCungCapModel TimTheoMaNhaCungCap(string v)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var Kh = ctx.NHACUNGCAP
                    .Where(p => p.MaNhaCungCap.Trim() == v.Trim())
                    .FirstOrDefault();
                NhaCungCapModel kq = new NhaCungCapModel();
                if (Kh != null)
                {
                    kq.MaNhaCungCap = Kh.MaNhaCungCap;
                    kq.DiDong = Kh.DiDong;
                    kq.DiaChi = Kh.DiaChi;
                    kq.DienThoai = Kh.DienThoai;
                }
                return kq;
            }
        }

        public static bool AddNhaCungCap(NHACUNGCAP model)
        {
            try
            {
                using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
                {
                    ctx.NHACUNGCAP.Add(model);
                    ctx.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
