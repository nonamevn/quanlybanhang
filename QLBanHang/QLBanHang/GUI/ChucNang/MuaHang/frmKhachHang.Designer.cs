﻿namespace QLBanHang
{
    partial class frmKhachHang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmKhachHang));
            this.rdDaiLy = new System.Windows.Forms.RadioButton();
            this.rdKhachLe = new System.Windows.Forms.RadioButton();
            this.chkConQuanLy = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txtNickYaHoo = new DevExpress.XtraEditors.TextEdit();
            this.txtNickSkype = new DevExpress.XtraEditors.TextEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.cbNoHienTai = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtWebsite = new DevExpress.XtraEditors.TextEdit();
            this.txtNganHang = new DevExpress.XtraEditors.TextEdit();
            this.txtFax = new DevExpress.XtraEditors.TextEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.txtMobile = new DevExpress.XtraEditors.TextEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.txtNguoiLienHe = new DevExpress.XtraEditors.TextEdit();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.txtTaiKhoan = new DevExpress.XtraEditors.TextEdit();
            this.txtMaSoThue = new DevExpress.XtraEditors.TextEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtDienThoai = new DevExpress.XtraEditors.TextEdit();
            this.txtTen = new DevExpress.XtraEditors.TextEdit();
            this.txtMa = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.lkKhuVuc = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnThemKhuVuc = new DevExpress.XtraEditors.SimpleButton();
            this.cbGioiHanNo = new DevExpress.XtraEditors.CalcEdit();
            this.cbChietKhau = new DevExpress.XtraEditors.CalcEdit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConQuanLy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNickYaHoo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNickSkype.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNoHienTai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNganHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNguoiLienHe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaiKhoan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaSoThue.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkKhuVuc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGioiHanNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbChietKhau.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // rdDaiLy
            // 
            this.rdDaiLy.AutoSize = true;
            this.rdDaiLy.Location = new System.Drawing.Point(24, 23);
            this.rdDaiLy.Margin = new System.Windows.Forms.Padding(6);
            this.rdDaiLy.Name = "rdDaiLy";
            this.rdDaiLy.Size = new System.Drawing.Size(104, 29);
            this.rdDaiLy.TabIndex = 0;
            this.rdDaiLy.TabStop = true;
            this.rdDaiLy.Text = "Đại Lý";
            this.rdDaiLy.UseVisualStyleBackColor = true;
            // 
            // rdKhachLe
            // 
            this.rdKhachLe.AutoSize = true;
            this.rdKhachLe.Location = new System.Drawing.Point(146, 23);
            this.rdKhachLe.Margin = new System.Windows.Forms.Padding(6);
            this.rdKhachLe.Name = "rdKhachLe";
            this.rdKhachLe.Size = new System.Drawing.Size(134, 29);
            this.rdKhachLe.TabIndex = 1;
            this.rdKhachLe.TabStop = true;
            this.rdKhachLe.Text = "Khách Lẻ";
            this.rdKhachLe.UseVisualStyleBackColor = true;
            // 
            // chkConQuanLy
            // 
            this.chkConQuanLy.Location = new System.Drawing.Point(890, 19);
            this.chkConQuanLy.Margin = new System.Windows.Forms.Padding(6);
            this.chkConQuanLy.Name = "chkConQuanLy";
            this.chkConQuanLy.Properties.Caption = "Còn Quản Lý";
            this.chkConQuanLy.Size = new System.Drawing.Size(208, 34);
            this.chkConQuanLy.TabIndex = 2;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnThemKhuVuc);
            this.groupControl1.Controls.Add(this.lkKhuVuc);
            this.groupControl1.Controls.Add(this.txtNickYaHoo);
            this.groupControl1.Controls.Add(this.txtNickSkype);
            this.groupControl1.Controls.Add(this.labelControl20);
            this.groupControl1.Controls.Add(this.labelControl21);
            this.groupControl1.Controls.Add(this.labelControl13);
            this.groupControl1.Controls.Add(this.cbNoHienTai);
            this.groupControl1.Controls.Add(this.txtWebsite);
            this.groupControl1.Controls.Add(this.txtNganHang);
            this.groupControl1.Controls.Add(this.txtFax);
            this.groupControl1.Controls.Add(this.labelControl14);
            this.groupControl1.Controls.Add(this.txtMobile);
            this.groupControl1.Controls.Add(this.labelControl15);
            this.groupControl1.Controls.Add(this.labelControl16);
            this.groupControl1.Controls.Add(this.labelControl19);
            this.groupControl1.Controls.Add(this.labelControl1);
            this.groupControl1.Controls.Add(this.labelControl11);
            this.groupControl1.Controls.Add(this.labelControl12);
            this.groupControl1.Controls.Add(this.txtNguoiLienHe);
            this.groupControl1.Controls.Add(this.txtEmail);
            this.groupControl1.Controls.Add(this.txtTaiKhoan);
            this.groupControl1.Controls.Add(this.txtMaSoThue);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.txtDiaChi);
            this.groupControl1.Controls.Add(this.labelControl2);
            this.groupControl1.Controls.Add(this.labelControl18);
            this.groupControl1.Controls.Add(this.labelControl17);
            this.groupControl1.Controls.Add(this.txtDienThoai);
            this.groupControl1.Controls.Add(this.txtTen);
            this.groupControl1.Controls.Add(this.txtMa);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.cbGioiHanNo);
            this.groupControl1.Controls.Add(this.cbChietKhau);
            this.groupControl1.Location = new System.Drawing.Point(2, 67);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(1116, 581);
            this.groupControl1.TabIndex = 3;
            this.groupControl1.Text = "Thông Tin Chung";
            // 
            // txtNickYaHoo
            // 
            this.txtNickYaHoo.Location = new System.Drawing.Point(770, 460);
            this.txtNickYaHoo.Margin = new System.Windows.Forms.Padding(6);
            this.txtNickYaHoo.Name = "txtNickYaHoo";
            this.txtNickYaHoo.Size = new System.Drawing.Size(336, 34);
            this.txtNickYaHoo.TabIndex = 65;
            // 
            // txtNickSkype
            // 
            this.txtNickSkype.Location = new System.Drawing.Point(770, 510);
            this.txtNickSkype.Margin = new System.Windows.Forms.Padding(6);
            this.txtNickSkype.Name = "txtNickSkype";
            this.txtNickSkype.Size = new System.Drawing.Size(336, 34);
            this.txtNickSkype.TabIndex = 64;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(588, 465);
            this.labelControl20.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(103, 25);
            this.labelControl20.TabIndex = 63;
            this.labelControl20.Text = "Nick Yahoo";
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(588, 515);
            this.labelControl21.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(101, 25);
            this.labelControl21.TabIndex = 62;
            this.labelControl21.Text = "Nick Skype";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(588, 423);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(110, 25);
            this.labelControl13.TabIndex = 61;
            this.labelControl13.Text = "Nợ Hiện Tại";
            // 
            // cbNoHienTai
            // 
            this.cbNoHienTai.Location = new System.Drawing.Point(770, 410);
            this.cbNoHienTai.Margin = new System.Windows.Forms.Padding(6);
            this.cbNoHienTai.Name = "cbNoHienTai";
            this.cbNoHienTai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbNoHienTai.Size = new System.Drawing.Size(336, 34);
            this.cbNoHienTai.TabIndex = 60;
            // 
            // txtWebsite
            // 
            this.txtWebsite.Location = new System.Drawing.Point(770, 310);
            this.txtWebsite.Margin = new System.Windows.Forms.Padding(6);
            this.txtWebsite.Name = "txtWebsite";
            this.txtWebsite.Size = new System.Drawing.Size(336, 34);
            this.txtWebsite.TabIndex = 59;
            // 
            // txtNganHang
            // 
            this.txtNganHang.Location = new System.Drawing.Point(770, 360);
            this.txtNganHang.Margin = new System.Windows.Forms.Padding(6);
            this.txtNganHang.Name = "txtNganHang";
            this.txtNganHang.Size = new System.Drawing.Size(336, 34);
            this.txtNganHang.TabIndex = 58;
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(770, 210);
            this.txtFax.Margin = new System.Windows.Forms.Padding(6);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(336, 34);
            this.txtFax.TabIndex = 57;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(586, 373);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(105, 25);
            this.labelControl14.TabIndex = 56;
            this.labelControl14.Text = "Ngân Hàng";
            // 
            // txtMobile
            // 
            this.txtMobile.Location = new System.Drawing.Point(770, 260);
            this.txtMobile.Margin = new System.Windows.Forms.Padding(6);
            this.txtMobile.Name = "txtMobile";
            this.txtMobile.Size = new System.Drawing.Size(336, 34);
            this.txtMobile.TabIndex = 55;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(588, 215);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(33, 25);
            this.labelControl15.TabIndex = 54;
            this.labelControl15.Text = "Fax";
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(586, 315);
            this.labelControl16.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(74, 25);
            this.labelControl16.TabIndex = 53;
            this.labelControl16.Text = "Website";
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(588, 265);
            this.labelControl19.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(60, 25);
            this.labelControl19.TabIndex = 52;
            this.labelControl19.Text = "Mobile";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(8, 515);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(133, 25);
            this.labelControl1.TabIndex = 51;
            this.labelControl1.Text = "Người Liên Hệ";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(8, 465);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(109, 25);
            this.labelControl11.TabIndex = 50;
            this.labelControl11.Text = "Chiết Khấu ";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(10, 415);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(113, 25);
            this.labelControl12.TabIndex = 49;
            this.labelControl12.Text = "Giới Hạn Nợ";
            // 
            // txtNguoiLienHe
            // 
            this.txtNguoiLienHe.Location = new System.Drawing.Point(192, 510);
            this.txtNguoiLienHe.Margin = new System.Windows.Forms.Padding(6);
            this.txtNguoiLienHe.Name = "txtNguoiLienHe";
            this.txtNguoiLienHe.Size = new System.Drawing.Size(336, 34);
            this.txtNguoiLienHe.TabIndex = 48;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(192, 310);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(6);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(336, 34);
            this.txtEmail.TabIndex = 45;
            // 
            // txtTaiKhoan
            // 
            this.txtTaiKhoan.Location = new System.Drawing.Point(192, 360);
            this.txtTaiKhoan.Margin = new System.Windows.Forms.Padding(6);
            this.txtTaiKhoan.Name = "txtTaiKhoan";
            this.txtTaiKhoan.Size = new System.Drawing.Size(336, 34);
            this.txtTaiKhoan.TabIndex = 44;
            // 
            // txtMaSoThue
            // 
            this.txtMaSoThue.Location = new System.Drawing.Point(192, 210);
            this.txtMaSoThue.Margin = new System.Windows.Forms.Padding(6);
            this.txtMaSoThue.Name = "txtMaSoThue";
            this.txtMaSoThue.Size = new System.Drawing.Size(336, 34);
            this.txtMaSoThue.TabIndex = 43;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(8, 373);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(93, 25);
            this.labelControl9.TabIndex = 42;
            this.labelControl9.Text = "Tài Khoản";
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl3.Location = new System.Drawing.Point(700, 65);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(27, 25);
            this.labelControl3.TabIndex = 41;
            this.labelControl3.Text = "(*)";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(588, 65);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(79, 25);
            this.labelControl8.TabIndex = 39;
            this.labelControl8.Text = "Khu Vực";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(192, 160);
            this.txtDiaChi.Margin = new System.Windows.Forms.Padding(6);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(914, 34);
            this.txtDiaChi.TabIndex = 37;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(8, 165);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(68, 25);
            this.labelControl2.TabIndex = 36;
            this.labelControl2.Text = "Địa Chỉ";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl18.Location = new System.Drawing.Point(122, 115);
            this.labelControl18.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(27, 25);
            this.labelControl18.TabIndex = 35;
            this.labelControl18.Text = "(*)";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl17.Location = new System.Drawing.Point(122, 65);
            this.labelControl17.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(27, 25);
            this.labelControl17.TabIndex = 34;
            this.labelControl17.Text = "(*)";
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Location = new System.Drawing.Point(192, 260);
            this.txtDienThoai.Margin = new System.Windows.Forms.Padding(6);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(336, 34);
            this.txtDienThoai.TabIndex = 31;
            // 
            // txtTen
            // 
            this.txtTen.Location = new System.Drawing.Point(192, 110);
            this.txtTen.Margin = new System.Windows.Forms.Padding(6);
            this.txtTen.Name = "txtTen";
            this.txtTen.Size = new System.Drawing.Size(914, 34);
            this.txtTen.TabIndex = 29;
            // 
            // txtMa
            // 
            this.txtMa.Location = new System.Drawing.Point(192, 60);
            this.txtMa.Margin = new System.Windows.Forms.Padding(6);
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(336, 34);
            this.txtMa.TabIndex = 28;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(10, 215);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(111, 25);
            this.labelControl10.TabIndex = 26;
            this.labelControl10.Text = "Mã Số Thuế";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(8, 315);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(51, 25);
            this.labelControl7.TabIndex = 25;
            this.labelControl7.Text = "Email";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(10, 265);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(101, 25);
            this.labelControl6.TabIndex = 24;
            this.labelControl6.Text = "Điện Thoại";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(8, 115);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(35, 25);
            this.labelControl5.TabIndex = 23;
            this.labelControl5.Text = "Tên";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(10, 65);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(27, 25);
            this.labelControl4.TabIndex = 22;
            this.labelControl4.Text = "Mã";
            // 
            // btnLuu
            // 
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.Location = new System.Drawing.Point(732, 681);
            this.btnLuu.Margin = new System.Windows.Forms.Padding(6);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(178, 44);
            this.btnLuu.TabIndex = 17;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnDong
            // 
            this.btnDong.Image = ((System.Drawing.Image)(resources.GetObject("btnDong.Image")));
            this.btnDong.Location = new System.Drawing.Point(922, 681);
            this.btnDong.Margin = new System.Windows.Forms.Padding(6);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(176, 44);
            this.btnDong.TabIndex = 18;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // lkKhuVuc
            // 
            this.lkKhuVuc.EditValue = "[Chon Khu Vuc]";
            this.lkKhuVuc.Location = new System.Drawing.Point(739, 60);
            this.lkKhuVuc.Margin = new System.Windows.Forms.Padding(6);
            this.lkKhuVuc.Name = "lkKhuVuc";
            this.lkKhuVuc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkKhuVuc.Properties.View = this.gridView6;
            this.lkKhuVuc.Size = new System.Drawing.Size(326, 34);
            this.lkKhuVuc.TabIndex = 56;
            // 
            // gridView6
            // 
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // btnThemKhuVuc
            // 
            this.btnThemKhuVuc.Image = ((System.Drawing.Image)(resources.GetObject("btnThemKhuVuc.Image")));
            this.btnThemKhuVuc.Location = new System.Drawing.Point(1059, 59);
            this.btnThemKhuVuc.Margin = new System.Windows.Forms.Padding(6);
            this.btnThemKhuVuc.Name = "btnThemKhuVuc";
            this.btnThemKhuVuc.Size = new System.Drawing.Size(37, 36);
            this.btnThemKhuVuc.TabIndex = 67;
            this.btnThemKhuVuc.Click += new System.EventHandler(this.btnThemKhuVuc_Click);
            // 
            // cbGioiHanNo
            // 
            this.cbGioiHanNo.Location = new System.Drawing.Point(192, 410);
            this.cbGioiHanNo.Margin = new System.Windows.Forms.Padding(6);
            this.cbGioiHanNo.Name = "cbGioiHanNo";
            this.cbGioiHanNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGioiHanNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.cbGioiHanNo.Properties.NullText = "[EditValue is null]";
            this.cbGioiHanNo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGioiHanNo.Size = new System.Drawing.Size(336, 34);
            this.cbGioiHanNo.TabIndex = 67;
            // 
            // cbChietKhau
            // 
            this.cbChietKhau.Location = new System.Drawing.Point(192, 460);
            this.cbChietKhau.Margin = new System.Windows.Forms.Padding(6);
            this.cbChietKhau.Name = "cbChietKhau";
            this.cbChietKhau.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbChietKhau.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.cbChietKhau.Properties.NullText = "[EditValue is null]";
            this.cbChietKhau.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbChietKhau.Size = new System.Drawing.Size(336, 34);
            this.cbChietKhau.TabIndex = 68;
            // 
            // frmKhachHang
            // 
            this.AcceptButton = this.btnLuu;
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 748);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.chkConQuanLy);
            this.Controls.Add(this.rdKhachLe);
            this.Controls.Add(this.rdDaiLy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmKhachHang";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thêm Khách Hàng";
            ((System.ComponentModel.ISupportInitialize)(this.chkConQuanLy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNickYaHoo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNickSkype.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbNoHienTai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWebsite.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNganHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMobile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNguoiLienHe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaiKhoan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaSoThue.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienThoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkKhuVuc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGioiHanNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbChietKhau.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rdDaiLy;
        private System.Windows.Forms.RadioButton rdKhachLe;
        private DevExpress.XtraEditors.CheckEdit chkConQuanLy;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.TextEdit txtNickYaHoo;
        private DevExpress.XtraEditors.TextEdit txtNickSkype;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.LabelControl labelControl21;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.ComboBoxEdit cbNoHienTai;
        private DevExpress.XtraEditors.TextEdit txtWebsite;
        private DevExpress.XtraEditors.TextEdit txtNganHang;
        private DevExpress.XtraEditors.TextEdit txtFax;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.TextEdit txtMobile;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.TextEdit txtNguoiLienHe;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.TextEdit txtTaiKhoan;
        private DevExpress.XtraEditors.TextEdit txtMaSoThue;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit txtDiaChi;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtDienThoai;
        private DevExpress.XtraEditors.TextEdit txtTen;
        private DevExpress.XtraEditors.TextEdit txtMa;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnThemKhuVuc;
        private DevExpress.XtraEditors.GridLookUpEdit lkKhuVuc;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.CalcEdit cbGioiHanNo;
        private DevExpress.XtraEditors.CalcEdit cbChietKhau;
    }
}