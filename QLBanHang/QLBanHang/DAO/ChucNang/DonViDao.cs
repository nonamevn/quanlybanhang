﻿using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class DonViDao
    {
        public List<DonViModel> DsDonVi()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.DONVITINH.Select(k => new DonViModel
                {
                    MaDonVi = k.MaDonVi,
                    TenDonVi = k.TenDonVi
                })
                .ToList();
                return list;
            }
        }
        public static bool AddDonViTinh(DONVITINH model)
        {
            try
            {
                using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
                {
                    ctx.DONVITINH.Add(model);
                    ctx.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
