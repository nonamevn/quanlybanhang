﻿namespace QLBanHang
{
    partial class PhieuBanHangControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhieuBanHangControl));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.dtNgayGiao = new DevExpress.XtraEditors.DateEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lkKhoHang = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtPhieu1 = new DevExpress.XtraEditors.TextEdit();
            this.cbMaKhachHang = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lkNhanVien = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.dtHanThanhToan = new DevExpress.XtraEditors.DateEdit();
            this.dtNgay = new DevExpress.XtraEditors.DateEdit();
            this.txtGhiChu = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.txtDiaChi = new System.Windows.Forms.TextBox();
            this.txtSoPhieuVietTay = new System.Windows.Forms.TextBox();
            this.txtSoHoaDonVat = new System.Windows.Forms.TextBox();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtDienThoai = new System.Windows.Forms.TextBox();
            this.cbbKhachHang = new DevExpress.XtraEditors.GridLookUpEdit();
            this.repositoryItemCalcEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvBanHang = new DevExpress.XtraGrid.GridControl();
            this.gridViewBanHang = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MaHangHoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lkMaHangHoa = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TenHangHoa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lkTenHangHoa = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.DonVi = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoLuong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.iSoLuong = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.DonGia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.calDonGia = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this.ThanhTien = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ChietKhau = new DevExpress.XtraGrid.Columns.GridColumn();
            this.calCK = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            this._ChietKhau = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ThanhToan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.btnTaoMoi = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnLuuVaThem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLuuVaDong = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnIn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnNapLai = new System.Windows.Forms.ToolStripButton();
            this.btnDong = new System.Windows.Forms.ToolStripButton();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cbDKTT = new System.Windows.Forms.ComboBox();
            this.cbHTTT = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgayGiao.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgayGiao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkKhoHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhieu1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMaKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkNhanVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHanThanhToan.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHanThanhToan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgay.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbKhachHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBanHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkMaHangHoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkTenHangHoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.calCK)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.AutoSize = true;
            this.groupControl1.Controls.Add(this.groupBox2);
            this.groupControl1.Controls.Add(this.groupBox1);
            this.groupControl1.Controls.Add(this.toolStrip1);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(2022, 915);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Phiếu Xuất Hàng";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbHTTT);
            this.groupBox2.Controls.Add(this.cbDKTT);
            this.groupBox2.Controls.Add(this.simpleButton1);
            this.groupBox2.Controls.Add(this.dtNgayGiao);
            this.groupBox2.Controls.Add(this.labelControl15);
            this.groupBox2.Controls.Add(this.lkKhoHang);
            this.groupBox2.Controls.Add(this.txtPhieu1);
            this.groupBox2.Controls.Add(this.cbMaKhachHang);
            this.groupBox2.Controls.Add(this.lkNhanVien);
            this.groupBox2.Controls.Add(this.dtHanThanhToan);
            this.groupBox2.Controls.Add(this.dtNgay);
            this.groupBox2.Controls.Add(this.txtGhiChu);
            this.groupBox2.Controls.Add(this.labelControl1);
            this.groupBox2.Controls.Add(this.labelControl14);
            this.groupBox2.Controls.Add(this.labelControl2);
            this.groupBox2.Controls.Add(this.labelControl10);
            this.groupBox2.Controls.Add(this.labelControl3);
            this.groupBox2.Controls.Add(this.labelControl11);
            this.groupBox2.Controls.Add(this.labelControl4);
            this.groupBox2.Controls.Add(this.labelControl12);
            this.groupBox2.Controls.Add(this.labelControl8);
            this.groupBox2.Controls.Add(this.labelControl13);
            this.groupBox2.Controls.Add(this.labelControl7);
            this.groupBox2.Controls.Add(this.labelControl6);
            this.groupBox2.Controls.Add(this.labelControl5);
            this.groupBox2.Controls.Add(this.txtDiaChi);
            this.groupBox2.Controls.Add(this.txtSoPhieuVietTay);
            this.groupBox2.Controls.Add(this.txtSoHoaDonVat);
            this.groupBox2.Controls.Add(this.labelControl9);
            this.groupBox2.Controls.Add(this.txtDienThoai);
            this.groupBox2.Controls.Add(this.cbbKhachHang);
            this.groupBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 79);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox2.Size = new System.Drawing.Size(2016, 306);
            this.groupBox2.TabIndex = 43;
            this.groupBox2.TabStop = false;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseBorderColor = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.Location = new System.Drawing.Point(846, 40);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(130, 38);
            this.simpleButton1.TabIndex = 61;
            this.simpleButton1.Text = "Tạo";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // dtNgayGiao
            // 
            this.dtNgayGiao.EditValue = null;
            this.dtNgayGiao.Location = new System.Drawing.Point(1214, 248);
            this.dtNgayGiao.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.dtNgayGiao.Name = "dtNgayGiao";
            this.dtNgayGiao.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtNgayGiao.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtNgayGiao.Size = new System.Drawing.Size(326, 34);
            this.dtNgayGiao.TabIndex = 59;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(1024, 256);
            this.labelControl15.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(95, 25);
            this.labelControl15.TabIndex = 58;
            this.labelControl15.Text = "Ngày Giao";
            // 
            // lkKhoHang
            // 
            this.lkKhoHang.Location = new System.Drawing.Point(1664, 208);
            this.lkKhoHang.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.lkKhoHang.Name = "lkKhoHang";
            this.lkKhoHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkKhoHang.Properties.View = this.gridView7;
            this.lkKhoHang.Size = new System.Drawing.Size(326, 34);
            this.lkKhoHang.TabIndex = 57;
            // 
            // gridView7
            // 
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            // 
            // txtPhieu1
            // 
            this.txtPhieu1.Location = new System.Drawing.Point(1664, 31);
            this.txtPhieu1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtPhieu1.Name = "txtPhieu1";
            this.txtPhieu1.Size = new System.Drawing.Size(326, 34);
            this.txtPhieu1.TabIndex = 56;
            // 
            // cbMaKhachHang
            // 
            this.cbMaKhachHang.Location = new System.Drawing.Point(1218, 37);
            this.cbMaKhachHang.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cbMaKhachHang.Name = "cbMaKhachHang";
            this.cbMaKhachHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbMaKhachHang.Properties.View = this.gridView6;
            this.cbMaKhachHang.Size = new System.Drawing.Size(326, 34);
            this.cbMaKhachHang.TabIndex = 55;
            this.cbMaKhachHang.TextChanged += new System.EventHandler(this.cbMaKhachHang_TextChanged);
            // 
            // gridView6
            // 
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // lkNhanVien
            // 
            this.lkNhanVien.EditValue = "";
            this.lkNhanVien.Location = new System.Drawing.Point(1664, 144);
            this.lkNhanVien.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.lkNhanVien.Name = "lkNhanVien";
            this.lkNhanVien.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkNhanVien.Properties.View = this.gridView2;
            this.lkNhanVien.Size = new System.Drawing.Size(326, 34);
            this.lkNhanVien.TabIndex = 50;
            // 
            // gridView2
            // 
            this.gridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            // 
            // dtHanThanhToan
            // 
            this.dtHanThanhToan.EditValue = null;
            this.dtHanThanhToan.Location = new System.Drawing.Point(762, 256);
            this.dtHanThanhToan.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.dtHanThanhToan.Name = "dtHanThanhToan";
            this.dtHanThanhToan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtHanThanhToan.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtHanThanhToan.Size = new System.Drawing.Size(250, 34);
            this.dtHanThanhToan.TabIndex = 49;
            // 
            // dtNgay
            // 
            this.dtNgay.EditValue = null;
            this.dtNgay.Location = new System.Drawing.Point(1664, 85);
            this.dtNgay.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.dtNgay.Name = "dtNgay";
            this.dtNgay.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtNgay.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtNgay.Size = new System.Drawing.Size(326, 34);
            this.dtNgay.TabIndex = 46;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(176, 148);
            this.txtGhiChu.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(836, 88);
            this.txtGhiChu.TabIndex = 45;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 46);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(113, 25);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Khách Hàng";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(712, 262);
            this.labelControl14.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(37, 25);
            this.labelControl14.TabIndex = 39;
            this.labelControl14.Text = "Hạn";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 100);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(68, 25);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Địa Chỉ";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(1556, 213);
            this.labelControl10.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(84, 25);
            this.labelControl10.TabIndex = 25;
            this.labelControl10.Text = "Kho Xuất";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 158);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(75, 25);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Ghi Chú";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(1556, 152);
            this.labelControl11.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(97, 25);
            this.labelControl11.TabIndex = 24;
            this.labelControl11.Text = "Nhân Viên";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 262);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(150, 25);
            this.labelControl4.TabIndex = 3;
            this.labelControl4.Text = "Đ.Khoản T.Toán";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(1556, 94);
            this.labelControl12.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(47, 25);
            this.labelControl12.TabIndex = 23;
            this.labelControl12.Text = "Ngày";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(1024, 46);
            this.labelControl8.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(147, 25);
            this.labelControl8.TabIndex = 4;
            this.labelControl8.Text = "Mã Khách Hàng";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(1556, 40);
            this.labelControl13.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(52, 25);
            this.labelControl13.TabIndex = 22;
            this.labelControl13.Text = "Phiếu";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(1024, 100);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(101, 25);
            this.labelControl7.TabIndex = 5;
            this.labelControl7.Text = "Điện Thoại";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(1024, 158);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(157, 25);
            this.labelControl6.TabIndex = 6;
            this.labelControl6.Text = "Số Hóa Đơn VAT";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(1024, 219);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(165, 25);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Số Phiếu Viết Tay";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(176, 90);
            this.txtDiaChi.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(832, 33);
            this.txtDiaChi.TabIndex = 9;
            // 
            // txtSoPhieuVietTay
            // 
            this.txtSoPhieuVietTay.Location = new System.Drawing.Point(1214, 202);
            this.txtSoPhieuVietTay.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtSoPhieuVietTay.Name = "txtSoPhieuVietTay";
            this.txtSoPhieuVietTay.Size = new System.Drawing.Size(322, 33);
            this.txtSoPhieuVietTay.TabIndex = 17;
            // 
            // txtSoHoaDonVat
            // 
            this.txtSoHoaDonVat.Location = new System.Drawing.Point(1214, 150);
            this.txtSoHoaDonVat.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtSoHoaDonVat.Name = "txtSoHoaDonVat";
            this.txtSoHoaDonVat.Size = new System.Drawing.Size(322, 33);
            this.txtSoHoaDonVat.TabIndex = 16;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(412, 262);
            this.labelControl9.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(51, 25);
            this.labelControl9.TabIndex = 13;
            this.labelControl9.Text = "HTTT";
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Location = new System.Drawing.Point(1214, 92);
            this.txtDienThoai.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(322, 33);
            this.txtDienThoai.TabIndex = 15;
            // 
            // cbbKhachHang
            // 
            this.cbbKhachHang.Location = new System.Drawing.Point(176, 40);
            this.cbbKhachHang.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.cbbKhachHang.Name = "cbbKhachHang";
            this.cbbKhachHang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbbKhachHang.Properties.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCalcEdit1});
            this.cbbKhachHang.Properties.View = this.gridLookUpEdit1View;
            this.cbbKhachHang.Size = new System.Drawing.Size(836, 34);
            this.cbbKhachHang.TabIndex = 56;
            this.cbbKhachHang.TextChanged += new System.EventHandler(this.cbbKhachHang_TextChanged);
            // 
            // repositoryItemCalcEdit1
            // 
            this.repositoryItemCalcEdit1.AutoHeight = false;
            this.repositoryItemCalcEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCalcEdit1.Name = "repositoryItemCalcEdit1";
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvBanHang);
            this.groupBox1.Location = new System.Drawing.Point(10, 410);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.groupBox1.Size = new System.Drawing.Size(2002, 438);
            this.groupBox1.TabIndex = 46;
            this.groupBox1.TabStop = false;
            // 
            // dgvBanHang
            // 
            this.dgvBanHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvBanHang.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvBanHang.Location = new System.Drawing.Point(6, 32);
            this.dgvBanHang.MainView = this.gridViewBanHang;
            this.dgvBanHang.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvBanHang.Name = "dgvBanHang";
            this.dgvBanHang.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.calDonGia,
            this.iSoLuong,
            this.calCK,
            this.lkTenHangHoa,
            this.lkMaHangHoa});
            this.dgvBanHang.Size = new System.Drawing.Size(1990, 400);
            this.dgvBanHang.TabIndex = 0;
            this.dgvBanHang.UseEmbeddedNavigator = true;
            this.dgvBanHang.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewBanHang});
            // 
            // gridViewBanHang
            // 
            this.gridViewBanHang.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.MaHangHoa,
            this.TenHangHoa,
            this.DonVi,
            this.SoLuong,
            this.DonGia,
            this.ThanhTien,
            this.ChietKhau,
            this._ChietKhau,
            this.ThanhToan});
            this.gridViewBanHang.GridControl = this.dgvBanHang;
            this.gridViewBanHang.Name = "gridViewBanHang";
            this.gridViewBanHang.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridViewBanHang.OptionsView.ShowGroupPanel = false;
            // 
            // MaHangHoa
            // 
            this.MaHangHoa.Caption = "Mã Hàng";
            this.MaHangHoa.ColumnEdit = this.lkMaHangHoa;
            this.MaHangHoa.FieldName = "MaHangHoa";
            this.MaHangHoa.Name = "MaHangHoa";
            this.MaHangHoa.Visible = true;
            this.MaHangHoa.VisibleIndex = 0;
            // 
            // lkMaHangHoa
            // 
            this.lkMaHangHoa.AutoHeight = false;
            this.lkMaHangHoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkMaHangHoa.DisplayMember = "MaHangHoa";
            this.lkMaHangHoa.Name = "lkMaHangHoa";
            this.lkMaHangHoa.ValueMember = "MaHangHoa";
            this.lkMaHangHoa.EditValueChanged += new System.EventHandler(this.lkMaHangHoa_EditValueChanged);
            // 
            // TenHangHoa
            // 
            this.TenHangHoa.Caption = "Tên Hàng";
            this.TenHangHoa.ColumnEdit = this.lkTenHangHoa;
            this.TenHangHoa.FieldName = "TenHangHoa";
            this.TenHangHoa.Name = "TenHangHoa";
            this.TenHangHoa.Visible = true;
            this.TenHangHoa.VisibleIndex = 1;
            // 
            // lkTenHangHoa
            // 
            this.lkTenHangHoa.AutoHeight = false;
            this.lkTenHangHoa.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkTenHangHoa.DisplayMember = "TenHangHoa";
            this.lkTenHangHoa.Name = "lkTenHangHoa";
            this.lkTenHangHoa.ValueMember = "MaHangHoa";
            this.lkTenHangHoa.EditValueChanged += new System.EventHandler(this.lkTenHangHoa_EditValueChanged);
            // 
            // DonVi
            // 
            this.DonVi.Caption = "Đơn Vị";
            this.DonVi.FieldName = "DonVi";
            this.DonVi.Name = "DonVi";
            this.DonVi.Visible = true;
            this.DonVi.VisibleIndex = 2;
            // 
            // SoLuong
            // 
            this.SoLuong.Caption = "Số Lượng";
            this.SoLuong.ColumnEdit = this.iSoLuong;
            this.SoLuong.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.SoLuong.FieldName = "SoLuong";
            this.SoLuong.Name = "SoLuong";
            this.SoLuong.Visible = true;
            this.SoLuong.VisibleIndex = 3;
            // 
            // iSoLuong
            // 
            this.iSoLuong.AutoHeight = false;
            this.iSoLuong.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.iSoLuong.Name = "iSoLuong";
            this.iSoLuong.EditValueChanged += new System.EventHandler(this.iSoLuong_EditValueChanged);
            // 
            // DonGia
            // 
            this.DonGia.Caption = "Đơn Giá";
            this.DonGia.ColumnEdit = this.calDonGia;
            this.DonGia.FieldName = "DonGia";
            this.DonGia.Name = "DonGia";
            this.DonGia.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.DonGia.Visible = true;
            this.DonGia.VisibleIndex = 4;
            // 
            // calDonGia
            // 
            this.calDonGia.AutoHeight = false;
            this.calDonGia.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calDonGia.Name = "calDonGia";
            // 
            // ThanhTien
            // 
            this.ThanhTien.Caption = "Thành Tiền";
            this.ThanhTien.FieldName = "ThanhTien";
            this.ThanhTien.Name = "ThanhTien";
            this.ThanhTien.OptionsColumn.ReadOnly = true;
            this.ThanhTien.Visible = true;
            this.ThanhTien.VisibleIndex = 5;
            // 
            // ChietKhau
            // 
            this.ChietKhau.Caption = "CK(%)";
            this.ChietKhau.ColumnEdit = this.calCK;
            this.ChietKhau.FieldName = "ChietKhau";
            this.ChietKhau.Name = "ChietKhau";
            this.ChietKhau.Visible = true;
            this.ChietKhau.VisibleIndex = 6;
            // 
            // calCK
            // 
            this.calCK.AutoHeight = false;
            this.calCK.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.calCK.Name = "calCK";
            this.calCK.EditValueChanged += new System.EventHandler(this.calCK_EditValueChanged);
            // 
            // _ChietKhau
            // 
            this._ChietKhau.Caption = "Chiết Khấu";
            this._ChietKhau.FieldName = "_ChietKhau";
            this._ChietKhau.Name = "_ChietKhau";
            this._ChietKhau.Visible = true;
            this._ChietKhau.VisibleIndex = 7;
            // 
            // ThanhToan
            // 
            this.ThanhToan.Caption = "Thanh Toán";
            this.ThanhToan.FieldName = "ThanhToan";
            this.ThanhToan.Name = "ThanhToan";
            this.ThanhToan.Visible = true;
            this.ThanhToan.VisibleIndex = 8;
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnTaoMoi,
            this.toolStripSeparator1,
            this.toolStripDropDownButton1,
            this.toolStripSeparator2,
            this.btnIn,
            this.toolStripSeparator3,
            this.btnNapLai,
            this.btnDong});
            this.toolStrip1.Location = new System.Drawing.Point(3, 40);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(2016, 39);
            this.toolStrip1.TabIndex = 44;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // btnTaoMoi
            // 
            this.btnTaoMoi.Image = ((System.Drawing.Image)(resources.GetObject("btnTaoMoi.Image")));
            this.btnTaoMoi.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnTaoMoi.Name = "btnTaoMoi";
            this.btnTaoMoi.Size = new System.Drawing.Size(139, 36);
            this.btnTaoMoi.Text = "Tạo Mới";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnLuuVaThem,
            this.btnLuuVaDong});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(208, 36);
            this.toolStripDropDownButton1.Text = "Lưu Và Thêm";
            // 
            // btnLuuVaThem
            // 
            this.btnLuuVaThem.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuVaThem.Image")));
            this.btnLuuVaThem.Name = "btnLuuVaThem";
            this.btnLuuVaThem.Size = new System.Drawing.Size(254, 38);
            this.btnLuuVaThem.Text = "Lưu Và Thêm";
            this.btnLuuVaThem.Click += new System.EventHandler(this.btnLuuVaThem_Click);
            // 
            // btnLuuVaDong
            // 
            this.btnLuuVaDong.Image = ((System.Drawing.Image)(resources.GetObject("btnLuuVaDong.Image")));
            this.btnLuuVaDong.Name = "btnLuuVaDong";
            this.btnLuuVaDong.Size = new System.Drawing.Size(254, 38);
            this.btnLuuVaDong.Text = "Lưu Và Đóng";
            this.btnLuuVaDong.Click += new System.EventHandler(this.btnLuuVaDong_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 39);
            // 
            // btnIn
            // 
            this.btnIn.Image = ((System.Drawing.Image)(resources.GetObject("btnIn.Image")));
            this.btnIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(71, 36);
            this.btnIn.Text = "In";
            this.btnIn.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // btnNapLai
            // 
            this.btnNapLai.Image = ((System.Drawing.Image)(resources.GetObject("btnNapLai.Image")));
            this.btnNapLai.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNapLai.Name = "btnNapLai";
            this.btnNapLai.Size = new System.Drawing.Size(131, 36);
            this.btnNapLai.Text = "Nạp Lại";
            // 
            // btnDong
            // 
            this.btnDong.Image = ((System.Drawing.Image)(resources.GetObject("btnDong.Image")));
            this.btnDong.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(110, 36);
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Mã Hàng";
            this.gridColumn1.ColumnEdit = this.lkMaHangHoa;
            this.gridColumn1.FieldName = "MaHangHoa";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // cbDKTT
            // 
            this.cbDKTT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDKTT.FormattingEnabled = true;
            this.cbDKTT.Location = new System.Drawing.Point(176, 257);
            this.cbDKTT.Name = "cbDKTT";
            this.cbDKTT.Size = new System.Drawing.Size(203, 33);
            this.cbDKTT.TabIndex = 1;
            // 
            // cbHTTT
            // 
            this.cbHTTT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbHTTT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHTTT.FormattingEnabled = true;
            this.cbHTTT.Location = new System.Drawing.Point(472, 257);
            this.cbHTTT.Name = "cbHTTT";
            this.cbHTTT.Size = new System.Drawing.Size(203, 33);
            this.cbHTTT.TabIndex = 62;
            // 
            // PhieuBanHangControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl1);
            this.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.Name = "PhieuBanHangControl";
            this.Size = new System.Drawing.Size(2022, 915);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgayGiao.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgayGiao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkKhoHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPhieu1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbMaKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkNhanVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHanThanhToan.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHanThanhToan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgay.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNgay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtGhiChu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbKhachHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCalcEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewBanHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkMaHangHoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkTenHangHoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.calCK)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.DateEdit dtNgayGiao;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.GridLookUpEdit lkKhoHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.TextEdit txtPhieu1;
        private DevExpress.XtraEditors.GridLookUpEdit cbMaKhachHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.GridLookUpEdit lkNhanVien;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraEditors.DateEdit dtHanThanhToan;
        private DevExpress.XtraEditors.DateEdit dtNgay;
        private DevExpress.XtraEditors.MemoEdit txtGhiChu;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.TextBox txtSoPhieuVietTay;
        private System.Windows.Forms.TextBox txtSoHoaDonVat;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private System.Windows.Forms.TextBox txtDienThoai;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripButton btnTaoMoi;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem btnLuuVaThem;
        private System.Windows.Forms.ToolStripMenuItem btnLuuVaDong;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton btnIn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton btnNapLai;
        private System.Windows.Forms.ToolStripButton btnDong;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private DevExpress.XtraGrid.GridControl dgvBanHang;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewBanHang;
        private DevExpress.XtraGrid.Columns.GridColumn TenHangHoa;
        private DevExpress.XtraGrid.Columns.GridColumn DonVi;
        private DevExpress.XtraGrid.Columns.GridColumn DonGia;
        private DevExpress.XtraGrid.Columns.GridColumn ThanhTien;
        private DevExpress.XtraGrid.Columns.GridColumn ChietKhau;
        private DevExpress.XtraGrid.Columns.GridColumn _ChietKhau;
        private DevExpress.XtraGrid.Columns.GridColumn ThanhToan;
        private DevExpress.XtraGrid.Columns.GridColumn SoLuong;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit iSoLuong;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit calDonGia;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit calCK;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkTenHangHoa;
        private DevExpress.XtraGrid.Columns.GridColumn MaHangHoa;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit lkMaHangHoa;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.GridLookUpEdit cbbKhachHang;
        private DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit repositoryItemCalcEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.ComboBox cbHTTT;
        private System.Windows.Forms.ComboBox cbDKTT;
    }
}
