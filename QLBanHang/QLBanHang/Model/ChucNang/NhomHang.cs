﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.ChucNang
{
    class NhomHang
    {
        public string MaNhomHang { get; set; }
        public string TenNhomHang { get; set; }
        public string GhiChu { get; set; }
        public Nullable<bool> ConQuanLyDonNhomHang { get; set; }
    }
}
