﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.ChucNang
{
    class PhieuMuaHang
    {
        public string MaPhieuNhapHang { get; set; }
        public string MaNhaCungCap { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public DateTime NgayNhap { get; set; }
        public string GhiChu { get; set; }
        public string NhanVien { get; set; }
        public string KhoNhap { get; set; }
        public string DieuKhoanThanhToan { get; set; }
        public string HinhThucThanhToan { get; set; }
        public DateTime HanThanhToan { get; set; }
    }
}
