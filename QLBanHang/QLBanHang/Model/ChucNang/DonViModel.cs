﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.ChucNang
{
    class DonViModel
    {
        public string MaDonVi { get; set; }
        public string TenDonVi { get; set; }
        public string GhiChu { get; set; }
    }
}
