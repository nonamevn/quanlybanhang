﻿using QuanLyBanHang.BUS.ChucNang;
using QuanLyBanHang.Model.Sesstion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmDoiMatKhauHeThong : Form
    {
        public frmDoiMatKhauHeThong()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            TaiKhoanBus bus = new TaiKhoanBus();
            if(txtMatKhauCu.Text=="" || txtMatKhauMoi.Text==""||txtMatKhauNhapLai.Text=="")
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin");
            }

            else if (txtMatKhauMoi.Text != txtMatKhauNhapLai.Text)
            {
                MessageBox.Show("Mật khẩu nhập lại không đúng");
            }
            else if (txtMatKhauCu.Text != bus.KiemTraMatKhauCu())
            {
                MessageBox.Show("Bạn đã nhập sai mật khẩu!");
                txtMatKhauCu.Refresh();
                txtMatKhauMoi.Refresh();
                txtMatKhauNhapLai.Refresh();
            }
            else
            {
                bus.DoiMatKhau(Session_NguoiDung_Model.TenDangNhap, txtMatKhauMoi.Text);
                MessageBox.Show("Đổi mật khẩu thành công!");
            }

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
