﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLBanHang.Model.DanhMuc;
using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model.DanhMuc;
using QuanLyBanHang.Model.Sesstion;

namespace QuanLyBanHang.BUS.ChucNang
{
    class TaiKhoanBus
    {
        TaiKhoanDao dao = new TaiKhoanDao();
        private bool session_NguoiDung_;

        public List<TaiKhoanModel> DsTaiKhoan()
        {
            return dao.DsTaiKhoan();
        }

        
        public static bool CheckLogin(string usr,string pas)
        {
            try
            {
                return TaiKhoanDao.CheckLogin(usr, pas);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public string KiemTraMatKhauCu()
        {
            return dao.LayMatKhau(Session_NguoiDung_Model.TenDangNhap);
        }
        public void DoiMatKhau(string taiKhoan,string matKhauMoi)
        {
            dao.DoiMatKhau(taiKhoan, matKhauMoi);
        }
        public string LayVaiTro(string taiKhoan)
        {
            return dao.LayVaiTro(taiKhoan);
        }
        
    }
}
