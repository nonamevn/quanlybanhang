﻿using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class NhaCungCapBus
    {
        NhaCungCapDao dao = new NhaCungCapDao();
        public List<NhaCungCapModel> DsNhaCungCap()
        {
            return dao.DsNhaCungCap();
        }

        public NhaCungCapModel TimTheoMaNhaCungCap(string v)
        {
            return dao.TimTheoMaNhaCungCap(v);
        }
        public static bool AddNhaCungCap(NHACUNGCAP model)
        {
            try
            {
                return NhaCungCapDao.AddNhaCungCap(model);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
