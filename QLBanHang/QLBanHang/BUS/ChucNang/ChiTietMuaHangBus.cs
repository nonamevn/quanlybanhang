﻿using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class ChiTietMuaHangBus
    {
        ChiTietNhapHangDao dao = new ChiTietNhapHangDao();
        public void ThemChiTiet(ChiTietPhieuNhapHang ct)
        {
            dao.ThemChiTiet(ct);
        }
    }
}
