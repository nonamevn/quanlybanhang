﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.ChucNang
{
    class PhieuBanHang
    {
        public string MaPhieuBanHang { get; set; }
        public string MaKhachHang { get; set; }
        public string DiaChi { get; set; }
        public string DienThoai { get; set; }
        public DateTime NgayBan { get; set; }
        public string GhiChu { get; set; }
        public string NhanVien { get; set; }
        public string KhoXuat { get; set; }
        public string DieuKhoanThanhToan { get; set; }
        public string HinhThucThanhToan { get; set; }
        public DateTime HanThanhToan { get; set; }
        public DateTime NgayGiao { get; set; }
        public decimal ThanhTien { get; set; }
    }
}
