﻿using DevExpress.XtraGrid.Columns;
using QuanLyBanHang.BUS.ChucNang;
using QuanLyBanHang.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmThemNhaCungCap : Form
    {
        public Action<String> GetNhaCungCapID;
        public frmThemNhaCungCap()
        {
            InitializeComponent();
            Load += FrmThemNhaCungCap_Load;
        }

        private void FrmThemNhaCungCap_Load(object sender, EventArgs e)
        {
            SetKhuVuc();
            loadData();
            txtMa.Text = MaNCCMoi();
        }
        private string MaNCCHienTai()
        {
            NhaCungCapBus a = new NhaCungCapBus();
            var c = a.DsNhaCungCap().OrderByDescending(p => int.Parse(p.MaNhaCungCap.Substring(3))).First();
            return c.MaNhaCungCap;
        }
        private string MaNCCMoi()
        {
            NhaCungCapBus a = new NhaCungCapBus();
            var c = a.DsNhaCungCap().OrderByDescending(p => int.Parse(p.MaNhaCungCap.Substring(3))).First();
            int MaMoi = int.Parse(c.MaNhaCungCap.Substring(3)) + 1;
            if (MaMoi < 10)
            {
                return "NCC00" + MaMoi;
            }
            else if (MaMoi < 100)
            {
                return "NCC0" + MaMoi;
            }
            else
            {
                return "NCC" + MaMoi;
            }
        }
        private void SetKhuVuc()
        {
            lkKhuVuc.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkKhuVuc.Properties.DisplayMember = "TenKhuVuc";
            lkKhuVuc.Properties.ValueMember = "MaKhuVuc";
            GridColumn col1 = lkKhuVuc.Properties.View.Columns.AddField("TenKhuVuc");
            col1.VisibleIndex = 0;
            col1.Caption = "Tên khu vực";
            GridColumn col2 = lkKhuVuc.Properties.View.Columns.AddField("MaKhuVuc");
            col2.VisibleIndex = 1;
            col2.Caption = "Mã khu vực";
            lkKhuVuc.Properties.View.BestFitColumns();
            lkKhuVuc.Properties.PopupFormWidth = 300;
        }
        private void loadData()
        {
            KhuVucBus kv = new KhuVucBus();
            lkKhuVuc.Properties.DataSource = kv.DsKhuVuc();
        }
        private void btnDong_Click(object sender, EventArgs e)
        {
            if (GetNhaCungCapID != null)
            {
                GetNhaCungCapID.Invoke(MaNCCHienTai());
            }
            Close();
        }

        private void btnThemKhuVuc_Click(object sender, EventArgs e)
        {
            frmThemKhuVuc frm = new frmThemKhuVuc();
            frm.GetMaKhuVuc = (String maKV) =>
              {
                  loadData();
                  lkKhuVuc.EditValue = maKV;
              };
            frm.ShowDialog();
        }
        private void KhoiTao()
        {
            txtMa.Text = MaNCCMoi();
            txtTen.Text = String.Empty;
            lkKhuVuc.Text = String.Empty;
            txtNguoiLienHe.Text = String.Empty;
            txtChucVu.Text = String.Empty;
            txtDiaChi.Text = String.Empty;
            txtMaSoThue.Text = String.Empty;
            txtDienThoai.Text = String.Empty;
            txtMobile.Text = String.Empty;
            txtFax.Text = String.Empty;
            txtEmail.Text = String.Empty;
            txtWebsite.Text = String.Empty;
            txtTaiKhoan.Text = String.Empty;
            txtNganHang.Text = String.Empty;
            calChietKhau.Value = 0;
            calGioiHanNo.Value = 0;
            calNoHienTai.Value = 0;
            chkConQuanLy.Checked = true;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            NHACUNGCAP model = new NHACUNGCAP();
            model.MaNhaCungCap = txtMa.Text;
            model.TenNhaCungCap = txtTen.Text;
            model.MaKhuVuc = lkKhuVuc.EditValue.ToString();
            model.NguoiLienHe = txtNguoiLienHe.Text;
            model.ChucVu = txtChucVu.Text;
            model.DiaChi = txtDiaChi.Text;
            model.MaSoThue = txtMaSoThue.Text;
            model.DienThoai = txtDienThoai.Text;
            model.DiDong = txtMobile.Text;
            model.Fax = txtFax.Text;
            model.Email = txtEmail.Text;
            model.Website = txtWebsite.Text;
            model.SoTaiKhoan = txtTaiKhoan.Text;
            model.TenNganHang = txtNganHang.Text;
            model.ChietKhau = calChietKhau.Value;
            model.GioiHanNo = calGioiHanNo.Value;
            model.NoHienTai = calNoHienTai.Value;
            model.ConQuanLyNhaCungCap = chkConQuanLy.Checked;
            try
            {
                if (NhaCungCapBus.AddNhaCungCap(model))
                {
                    MessageBox.Show("Thêm thành công");
                    KhoiTao();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
