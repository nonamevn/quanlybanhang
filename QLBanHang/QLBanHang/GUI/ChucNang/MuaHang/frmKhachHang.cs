﻿using DevExpress.XtraGrid.Columns;
using QLBanHang.Model;
using QuanLyBanHang.BUS.ChucNang;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmKhachHang : Form
    {
        KhuVucBus busKhuVuc = new KhuVucBus();
        KhachHangBus busKhachHang = new KhachHangBus();
        public Action<String> GetKhachHang;
        public frmKhachHang()
        {
            InitializeComponent();
            Load += FrmKhachHang_Load;
            
            FormClosed += FrmKhachHang_FormClosed;
        }

        private void FrmKhachHang_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }


        //---------------
           /*Hàm set thuộc tính*/
        //---------------
        private void SetDataSoure_DispPlayMember_ValueMember()
        {
            lkKhuVuc.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkKhuVuc.Properties.DisplayMember = "TenKhuVuc";
            lkKhuVuc.Properties.ValueMember = "MaKhuVuc";
            GridColumn col1 = lkKhuVuc.Properties.View.Columns.AddField("MaKhuVuc");
            col1.VisibleIndex = 0;
            col1.Caption = "Mã Khu Vực";
            GridColumn col2 = lkKhuVuc.Properties.View.Columns.AddField("TenKhuVuc");
            col2.VisibleIndex = 1;
            col2.Caption = "Tên Khu Vực";
            lkKhuVuc.Properties.View.BestFitColumns();
            lkKhuVuc.Properties.PopupFormWidth = 300;
        }

        private void LoadKhuVuc()
        {
            lkKhuVuc.Properties.DataSource = busKhuVuc.DsKhuVuc();
        }
        private void TaoMaKhachHangMoi()
        {
            string id = busKhachHang.MaKhachHangMoi();
            txtMa.Text = id;
            txtMa.ReadOnly = true;
        }
        private void FrmKhachHang_Load(object sender, EventArgs e)
        {
            SetDataSoure_DispPlayMember_ValueMember();
            LoadKhuVuc();
            TaoMaKhachHangMoi();
            cbNoHienTai.ReadOnly = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            Close();
        }
        private bool KiemTra()
        {
            if(txtTen.Text == string.Empty)
            {
                MessageBox.Show("Bạn chưa nhập tên");
                return false;
            }
            if(lkKhuVuc.Text == string.Empty)
            {
                MessageBox.Show("Bạn chưa chọn khu vực");
                return false;
            }
            return true;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(KiemTra() == true)
            {
                KhachHangModel kh = new KhachHangModel();
                kh.MaKhachHang = txtMa.Text;
                kh.TenKhachHang = txtTen.Text;
                kh.MaKhuVuc = lkKhuVuc.EditValue.ToString();
                kh.DiaChi = txtDiaChi.Text;
                kh.MaSoThue = txtMaSoThue.Text;
                kh.DienThoai = txtDienThoai.Text;
                kh.Email = txtEmail.Text;
                kh.SoTaiKhoan = txtTaiKhoan.Text;
                kh.Fax = txtFax.Text;
                kh.DiDong = txtMobile.Text;
                kh.Website = txtWebsite.Text;
                kh.TenNganHang = txtNganHang.Text;
                kh.NickYaHoo = txtNickYaHoo.Text;
                kh.NickSkype = txtNickSkype.Text;
                kh.NguoiLienHe = txtNguoiLienHe.Text;
                try
                {
                    kh.GioiHanNo = decimal.Parse(cbGioiHanNo.Text);
                }
                catch
                {
                    kh.GioiHanNo = 0;
                }
                try
                {
                    kh.ChietKhau = decimal.Parse(cbChietKhau.Text);
                }
                catch
                {
                    kh.ChietKhau = 0;
                }
                if(chkConQuanLy.Checked == true)
                {
                    kh.ConQuanLyKhachHang = true;
                }
                else
                {
                    kh.ConQuanLyKhachHang = false;
                }
                busKhachHang.ThemKhachHang(kh);
                MessageBox.Show("Thêm thành công","Thông báo");
                if (GetKhachHang != null)
                {
                    GetKhachHang.Invoke(txtMa.Text);
                }
                Close();
            }
        }

        private void btnThemKhuVuc_Click(object sender, EventArgs e)
        {
            frmThemKhuVuc frm = new frmThemKhuVuc();
            frm.GetMaKhuVuc = (String maKV) =>
            {
                LoadKhuVuc();
                lkKhuVuc.EditValue = maKV;
            };
            frm.ShowDialog();
        }
    }
}
