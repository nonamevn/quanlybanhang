﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.DanhMuc
{
    class TaiKhoanModel
    {
        public string MaTaiKhoan { get; set; }
        public string MaNhanVien { get; set; }
        public string TenDangNhap { get; set; }
        public string MatKhau { get; set; }
        public string MaVaiTro { get; set; }
        public string DienGiai { get; set; }

    }
}
