﻿using QLBanHang.Model;
using QuanLyBanHang.DAO.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class KhoHangBus
    {
        KhoHangDao dao = new KhoHangDao();
        public void ThemKhoHang(KhoHangModel kh)
        {
            dao.ThemKhoHang(kh);
        }
        public string MaKhoHangMoi()
        {
            var list = dao.DsKhoHang();
            List<string> ds = new List<string>();
            foreach (var item in list)
            {
                ds.Add(item.MaKho);
            }
            List<int> ds_id = new List<int>();
            foreach (var rs in ds)
            {
                rs.Trim();
                ds_id.Add(Int32.Parse(rs.Substring(1)));
            }
            ds_id.Sort();
            int id = ds_id.Count();
            int i;
            for (i = 0; i < ds_id.Count(); i++)
            {
                if ((i + 1) != ds_id[i])
                {

                    break;
                }
            }
            id = i + 1;
            string s = "";
            if (id < 10)
            {
                s = "K00" + id.ToString();
            }
            else if (id < 100)
            {
                s = "K0" + id.ToString();
            }
            else
            {
                s = "K" + id.ToString();
            }
            return s;
        }

        internal List<KhoHangModel> DsKhoHang()
        {
            return dao.DsKhoHang();
        }
    }
}
