﻿using QuanLyBanHang.BUS.ChucNang;
using QuanLyBanHang.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmThemDonViTinh : Form
    {
        public Action<String> GetMaDonViTinh;
        public frmThemDonViTinh()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            if(GetMaDonViTinh !=null)
            {
                GetMaDonViTinh.Invoke(txtMa.Text.Trim());
            }
            Close();
        }
        private bool validate()
        {
            if (string.IsNullOrEmpty(txtMa.Text) || string.IsNullOrWhiteSpace(txtMa.Text.Trim()))
            {
                MessageBox.Show("Mã Nhóm Hàng là bắt buộc");
                return false;
            }
            if (string.IsNullOrEmpty(txtTen.Text) || string.IsNullOrWhiteSpace(txtTen.Text.Trim()))
            {
                MessageBox.Show("Tên Nhóm Hàng là bắt buộc");
                return false;
            }
            if (string.IsNullOrEmpty(txtGhiChu.Text) || string.IsNullOrWhiteSpace(txtGhiChu.Text.Trim()))
            {
                MessageBox.Show("Ghi Chú là bắt buộc");
                return false;
            }
            return true;
        }

        private void btnLuu_Click_1(object sender, EventArgs e)
        {
            DONVITINH model = new DONVITINH();
            model.MaDonVi = txtMa.Text.Trim();
            model.TenDonVi = txtTen.Text.Trim();
            model.GhiChu = txtGhiChu.Text.Trim();
            model.ConQuanLyDonViTinh = chkConQuanLy.Checked;
            if (validate())
            {
                try
                {
                    if (DonViBus.AddDonViTinh(model))
                    {
                        MessageBox.Show("Thêm thành công");
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
