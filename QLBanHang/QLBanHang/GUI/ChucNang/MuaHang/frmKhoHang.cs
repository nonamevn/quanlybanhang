﻿using DevExpress.XtraGrid.Columns;
using QLBanHang.Model;
using QuanLyBanHang.BUS.ChucNang;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmKhoHang : Form
    {
        KhoHangBus busKhoHang = new KhoHangBus();
        NhanVienBus busNhanVien = new NhanVienBus();
        public Action<String> GetKhoHang;
        public frmKhoHang()
        {
            InitializeComponent();
            Load += FrmKhoHang_Load;
        }
        private void MaKhoHangMoi()
        {
            txtMa.ReadOnly = true;
            string id = busKhoHang.MaKhoHangMoi();
            txtMa.Text = id;
        }
        private void LoadNguoiQuanLy()
        {
            List<NhanVienModel> list = busNhanVien.DsNhanVien();
            lkNguoiQuanLy.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkNguoiQuanLy.Properties.DataSource = list;
            lkNguoiQuanLy.Properties.DisplayMember = "HoTen";
            lkNguoiQuanLy.Properties.ValueMember = "MaNhanVien";

            GridColumn col1 = lkNguoiQuanLy.Properties.View.Columns.AddField("MaNhanVien");
            col1.VisibleIndex = 0;
            col1.Caption = "Mã";

            GridColumn col2 = lkNguoiQuanLy.Properties.View.Columns.AddField("HoTen");
            col2.VisibleIndex = 1;
            col2.Caption = "Tên";

            lkNguoiQuanLy.Properties.View.BestFitColumns();
            lkNguoiQuanLy.Properties.PopupFormWidth = 300;
        }
        private void FrmKhoHang_Load(object sender, EventArgs e)
        {
            MaKhoHangMoi();
            LoadNguoiQuanLy();
            chkConQuanLy.Checked = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            Close();
        }
        private bool KiemTra()
        {
            if(txtTen.Text == string.Empty)
            {
                MessageBox.Show("Chưa nhập tên kho hàng","Thông báo");
                return false;
            }
            if(lkNguoiQuanLy.Text == string.Empty)
            {
                MessageBox.Show("Chưa chọn người quản lý");
            }
            return true;



        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(KiemTra() == true)
            {
                KhoHangModel kh = new KhoHangModel();
                kh.MaKho = txtMa.Text;
                kh.TenKho = txtTen.Text;
                kh.KyHieu = txtKyHieu.Text;
                kh.NguoiQuanLy = lkNguoiQuanLy.EditValue.ToString();
                kh.NguoiLienHe = txtNguoiLH.Text;
                kh.DiaChi = txtDiaChi.Text;
                kh.Fax = txtFax.Text;
                kh.DienThoai = txtDienThoai.Text;
                kh.Email = txtEmail.Text;
                kh.GhiChu = txtDienGia.Text;
                if(chkConQuanLy.Checked == true)
                {
                    kh.ConQuanLyKhoHang = true;
                }
                else
                {
                    kh.ConQuanLyKhoHang = false;
                }
                busKhoHang.ThemKhoHang(kh);
                MessageBox.Show("Thêm thành công","Thông báo");
                if (GetKhoHang != null)
                {
                    GetKhoHang.Invoke(txtMa.Text);
                }
            }
        }
    }
}
