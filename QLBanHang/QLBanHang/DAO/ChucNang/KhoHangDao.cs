﻿using QLBanHang.Model;
using QuanLyBanHang.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class KhoHangDao
    {
        public List<KhoHangModel> DsKhoHang()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.KHOHANG.Select(k => new KhoHangModel
                {
                    MaKho = k.MaKho,
                    TenKho = k.TenKho
                })
                .ToList();
                return list;
            }
        }
        public void ThemKhoHang(KhoHangModel k)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                KHOHANG kh = new KHOHANG();
                kh.MaKho = k.MaKho;
                kh.TenKho = k.TenKho;
                kh.KyHieu = k.KyHieu;
                kh.NguoiQuanLy = k.NguoiQuanLy;
                kh.NguoiLienHe = k.NguoiLienHe;
                kh.DiaChi = k.DiaChi;
                kh.Fax = k.Fax;
                kh.DienThoai = k.DienThoai;
                kh.Email = k.Email;
                kh.GhiChu = k.GhiChu;
                ctx.KHOHANG.Add(kh);
                ctx.SaveChanges();
            }
        }
    }
}
