﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHang.Model.ChucNang;
using QuanLyBanHang.Model;

namespace QuanLyBanHang.DAO.ChucNang
{
    class ChiTietNhapHangDao
    {
        public void ThemChiTiet(ChiTietPhieuNhapHang c)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                CHITIETPHIEUNHAPHANG ct = new CHITIETPHIEUNHAPHANG();
                ct.MaPhieuNhapHang = c.MaPhieuNhapHang;
                ct.MaChiTietPhieuNhapHang = c.MaChiTietPhieuNhapHang;
                ct.MaHangHoa = c.MaHangHoa;
                ct.SoLuong = c.SoLuong;
                ct.ThanhTien = decimal.Parse(c.ThanhTien.ToString());
                ctx.CHITIETPHIEUNHAPHANG.Add(ct);
                ctx.SaveChanges();
            }
        }
    }
}
