﻿using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class NhomHangBus
    {
        NhomHangDao dao = new NhomHangDao();
        public List<NhomHang> DsNhomHang()
        {
            return dao.DsNhomHang();
        }
        public static bool AddNhomHang(NHOMHANG model)
        {
            try
            {
                return NhomHangDao.AddNhomHang(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
