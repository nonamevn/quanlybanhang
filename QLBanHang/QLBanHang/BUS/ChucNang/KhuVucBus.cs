﻿
using QLBanHang.Model;
using QuanLyBanHang.DAO.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class KhuVucBus
    {
        KhuVucDao dao = new KhuVucDao();
        public List<KhuVucModel> DsKhuVuc()
        {
            return dao.DsKhuVuc();
        }
        public string MaKhuVucMoi()
        {
            var list = dao.DsKhuVuc();
            List<string> ds = new List<string>();
            foreach (var item in list)
            {
                ds.Add(item.MaKhuVuc);
            }
            List<int> ds_id = new List<int>();
            foreach (var rs in ds)
            {
                rs.Trim();
                ds_id.Add(Int32.Parse(rs.Substring(2)));
            }
            ds_id.Sort();
            int id = ds_id.Count();
            int i;
            for (i = 0; i < ds_id.Count(); i++)
            {
                if ((i + 1) != ds_id[i])
                {

                    break;
                }
            }
            id = i + 1;
            string s = "";
            if (id < 10)
            {
                s = "KV00" + id.ToString();
            }
            else if(id < 100)
            {
                s = "KV0" + id.ToString();
            }
            else
            {
                s = "KV" + id.ToString();
            }
            return s;
        }
        public void ThemKhuVuc(KhuVucModel kv)
        {
            dao.ThemKhuVuc(kv);
        }
    }
}
