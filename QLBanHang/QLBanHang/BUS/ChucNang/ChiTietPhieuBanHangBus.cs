﻿using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class ChiTietPhieuBanHangBus
    {
        ChiTietPhieuBanHangDao dao = new ChiTietPhieuBanHangDao();
        public void ThemChiTiet(ChiTietPhieuBanHang ct)
        {
            dao.ThemChiTiet(ct);
        }
    }
}
