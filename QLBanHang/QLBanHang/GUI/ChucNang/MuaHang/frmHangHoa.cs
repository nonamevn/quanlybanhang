﻿using DevExpress.XtraGrid.Columns;
using QuanLyBanHang.BUS.ChucNang;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmHangHoa : Form
    {
        HangHoaBus busHangHoa = new HangHoaBus();
        public Action<String> GetHangHoa;
        public frmHangHoa()
        {
            InitializeComponent();
            Load += FrmHangHoa_Load;
        }
        private void SetDataSoure_DispPlayMember_ValueMember()
        {
            lkKho.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkKho.Properties.DisplayMember = "TenKho";
            lkKho.Properties.ValueMember = "MaKho";
            GridColumn col1 = lkKho.Properties.View.Columns.AddField("TenKho");
            col1.VisibleIndex = 0;
            col1.Caption = "Tên kho";
            GridColumn col2 = lkKho.Properties.View.Columns.AddField("MaKho");
            col2.VisibleIndex = 1;
            col2.Caption = "Mã";
            lkKho.Properties.View.BestFitColumns();
            lkKho.Properties.PopupFormWidth = 300;
            //nhom hang
            lkPhanLoai.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkPhanLoai.Properties.DisplayMember = "TenNhomHang";
            lkPhanLoai.Properties.ValueMember = "MaNhomHang";
            GridColumn nh1 = lkPhanLoai.Properties.View.Columns.AddField("TenNhomHang");
            nh1.VisibleIndex = 0;
            nh1.Caption = "Nhóm hàng";
            GridColumn nh2 = lkPhanLoai.Properties.View.Columns.AddField("MaNhomHang");
            nh2.VisibleIndex = 1;
            nh2.Caption = "Mã";
            lkPhanLoai.Properties.View.BestFitColumns();
            lkPhanLoai.Properties.PopupFormWidth = 300;
            //Nha cung cap
            lkNhaCungCap.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkNhaCungCap.Properties.DisplayMember = "TenNhaCungCap";
            lkNhaCungCap.Properties.ValueMember = "MaNhaCungCap";
            GridColumn nc1 = lkNhaCungCap.Properties.View.Columns.AddField("TenNhaCungCap");
            nc1.VisibleIndex = 0;
            nc1.Caption = "Tên nhà cung cấp";
            GridColumn nc2 = lkNhaCungCap.Properties.View.Columns.AddField("MaNhaCungCap");
            nc2.VisibleIndex = 1;
            nc2.Caption = "Mã";
            lkNhaCungCap.Properties.View.BestFitColumns();
            lkNhaCungCap.Properties.PopupFormWidth = 300;
            //don vi
            lkDonVi.Properties.View.OptionsBehavior.AutoPopulateColumns = false;
            lkDonVi.Properties.DisplayMember = "TenDonVi";
            lkDonVi.Properties.ValueMember = "MaDonVi";
            GridColumn dv1 = lkDonVi.Properties.View.Columns.AddField("TenDonVi");
            dv1.VisibleIndex = 0;
            dv1.Caption = "Tên đơn vị";
            GridColumn dv2 = lkDonVi.Properties.View.Columns.AddField("MaDonVi");
            dv2.VisibleIndex = 1;
            dv2.Caption = "Mã";
            lkDonVi.Properties.View.BestFitColumns();
            lkDonVi.Properties.PopupFormWidth = 300;
            //
        }
        private void LoadNhomHang()
        {
            NhomHangBus bus = new NhomHangBus();
            lkPhanLoai.Properties.DataSource = bus.DsNhomHang();
        }
        private void LoadKho()
        {
            KhoHangBus bus = new KhoHangBus();
            lkKho.Properties.DataSource = bus.DsKhoHang();
        }
        private void LoadNhaCungCap()
        {
            NhaCungCapBus bus = new NhaCungCapBus();
            lkNhaCungCap.Properties.DataSource = bus.DsNhaCungCap();
        }
        private void LoadLoaiHang()
        {
            cbLoaiHang.Items.Add("Hàng hóa");
            cbLoaiHang.Items.Add("Dịch vụ");
        }
        private void LoadDonVi()
        {
            DonViBus bus = new DonViBus();
            lkDonVi.Properties.DataSource = bus.DsDonVi();
        }
        private void MaHoangHoaMoi()
        {
            txtMHang.Text = busHangHoa.MaHangHoaMoi();
            txtMHang.ReadOnly = true;
        }
        private void FrmHangHoa_Load(object sender, EventArgs e)
        {
            SetDataSoure_DispPlayMember_ValueMember();
            LoadNhomHang();
            LoadKho();
            LoadNhaCungCap();
            LoadLoaiHang();
            LoadDonVi();
            MaHoangHoaMoi();
            
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            QuanLyBanHang.Model.HANGHOA hh = new QuanLyBanHang.Model.HANGHOA();
            hh.MaHangHoa = txtMHang.Text;
            hh.TenHangHoa = txtTenHang.Text;
            hh.LoaiHang = cbLoaiHang.Text;
            hh.MaKho = lkKho.EditValue.ToString();
            hh.MaNhomHang = lkPhanLoai.EditValue.ToString();
            hh.MaDonVi = lkDonVi.EditValue.ToString();
            hh.VachNhaSX = txtMaVach.Text;
            hh.XuatXu = txtXuatXu.Text;
            hh.MaNhaCungCap = lkNhaCungCap.EditValue.ToString();
            try
            {
                hh.GiaBanLe = decimal.Parse(cbGiaBanLe.Text);
            }
            catch
            {
                hh.GiaBanLe = 0;
            }
            try
            {
                hh.GiaMua = decimal.Parse(cbGiaMua.Text);
            }
            catch
            {
                hh.GiaMua = 0;
            }
            try
            {
                hh.GiaBanSi = decimal.Parse(cbGiaBanSi.Text);
            }
            catch
            {
                hh.GiaBanSi = 0;
            }
            try
            {
                if (Int32.Parse(cbTonKhoToiThieu.Text) < 0)
                {
                    hh.TonKhoToiThieu = 0;
                }
                else
                {
                    hh.TonKhoToiThieu = Int32.Parse(cbTonKhoToiThieu.Text);
                }
            }
            catch
            {
                hh.TonKhoToiThieu = 0;
            }
            try
            {
                if (Int32.Parse(cbTonKhoToiThieu.Text) < 0)
                {
                    hh.TonKhoHienTai = 0;
                }
                else
                {
                    hh.TonKhoHienTai = Int32.Parse(cbTonHienTai.Text);
                }
            }
            catch
            {
                hh.TonKhoHienTai = 0;
            }
            if (chkConQuanLy.Checked == true)
            {
                hh.ConQuanLyHangHoa = true;
            }
            else
            {
                hh.ConQuanLyHangHoa = false;
            }
            try
            {
                busHangHoa.ThemHangHoa(hh);
                MessageBox.Show("Thêm thành công");
            }
            catch
            {

            }
            if (GetHangHoa != null)
            {
                GetHangHoa.Invoke(txtMHang.Text);
            }

            Close();
        }

        private void btnThemKhuVuc_Click(object sender, EventArgs e)
        {
            frmKhoHang frm = new frmKhoHang();
            frm.GetKhoHang = (String maKH) =>
            {
                LoadKho();
                lkKho.EditValue = maKH;
            };
            frm.ShowDialog();
        }

        private void btnThemPhanLoai_Click(object sender, EventArgs e)
        {
            frmThemNhomHangHoa frm = new frmThemNhomHangHoa();
            frm.GetMaNhomHang = (String maNhonHang) =>
            {
                LoadNhomHang();
                lkPhanLoai.EditValue = maNhonHang;
            };
            frm.ShowDialog();
        }

        private void btnThemDonVi_Click(object sender, EventArgs e)
        {
            frmThemDonViTinh frm = new frmThemDonViTinh();
            frm.GetMaDonViTinh = (String maDVT) =>
             {
                 LoadDonVi();
                 lkDonVi.EditValue = maDVT;
             };
            frm.ShowDialog();
        }

        private void btnThemNhaCungCap_Click(object sender, EventArgs e)
        {
            frmThemNhaCungCap frm = new frmThemNhaCungCap();
            frm.GetNhaCungCapID = (String maNCCID) =>
             {
                 LoadNhaCungCap();
                 lkNhaCungCap.EditValue = maNCCID;
             };
            frm.ShowDialog();
        }
        private string maxMaHang()
        {
            // return busHangHoa.DsHangHoa().Max().ToString();
            return "";
        }
    }
}
