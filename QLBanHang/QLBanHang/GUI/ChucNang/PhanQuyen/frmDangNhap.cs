﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyBanHang.BUS.ChucNang;
using QLBanHang;

namespace QuanLyBanHang.GUI.ChucNang.PhanQuyen
{
    public partial class frmDangNhap : Form
    {
        public frmDangNhap()
        {
            InitializeComponent();
            txtTaiKhoan.Text = Properties.Settings.Default.user;
            txtMatKhau.Text = Properties.Settings.Default.pass;
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
           
        }

        private void btnKetThuc_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //public Boolean KiemTraDangNhap()
        //{

        //    return true;
        //}

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            string tendangnhap = txtTaiKhoan.Text.Trim();
            string matkhau = txtMatKhau.Text.Trim();
            if (tendangnhap.Length == 0)
            {
                MessageBox.Show("Bạn chưa nhập tài khoản");
                txtTaiKhoan.Focus();
            }

            else if (matkhau.Length == 0)
            {
                MessageBox.Show("Bạn chưa nhập mật khẩu");
                txtMatKhau.Focus();
            }

            else if (tendangnhap.Length == 0 && matkhau.Length == 0)
            {
                MessageBox.Show("Vui lòng nhập vào Tài Khoản và Mật Khẩu");
                txtTaiKhoan.Focus();
            }
            else
            {
                var bus = new TaiKhoanBus();
                try
                {
                    if (TaiKhoanBus.CheckLogin(txtTaiKhoan.Text.Trim(), txtMatKhau.Text.Trim()))
                    {
                        MessageBox.Show("Chúc mừng đăng nhập thành công!");
                        if (cbLuuTKMK.Checked)
                        {
                            Properties.Settings.Default.user = txtTaiKhoan.Text;
                            Properties.Settings.Default.pass = txtMatKhau.Text;
                            Properties.Settings.Default.Save();
                        }
                        else
                        {
                            Properties.Settings.Default.user = "";
                            Properties.Settings.Default.pass = "";
                            Properties.Settings.Default.Save();
                        }
                        this.Hide();
                        frmPhanMemQuanLyBanHang main = new frmPhanMemQuanLyBanHang();
                        main.ShowDialog();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Bạn đã nhập sai Tài Khoản hoặc Mật Khẩu. Vui lòng nhập lại!");
                        txtTaiKhoan.Refresh();
                        txtMatKhau.Refresh();
                        txtTaiKhoan.Focus();
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                

            }
            
        }
    }
}
