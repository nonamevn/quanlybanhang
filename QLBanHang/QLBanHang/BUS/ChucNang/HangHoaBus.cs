﻿using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class HangHoaBus
    {
        HangHoaDao dao = new HangHoaDao();
        public List<HangHoaModel> DsHangHoa()
        {
            return dao.DsHangHoa();
        }

        public HangHoaModel TimTheoMaHH(string MaHH)
        {
            return dao.TimTheoMaHH(MaHH);
        }
        public string TimMa(String Ten)
        {
            return dao.TimMa(Ten);
        }
        public string MaHangHoaMoi()
        {
            var list = dao.DsHangHoa();
            List<string> ds = new List<string>();
            foreach (var item in list)
            {
                ds.Add(item.MaHangHoa);
            }
            List<int> ds_id = new List<int>();
            foreach (var rs in ds)
            {
                rs.Trim();
                ds_id.Add(Int32.Parse(rs.Substring(2)));
            }
            ds_id.Sort();
            int id = ds_id.Count();
            int i;
            for (i = 0; i < ds_id.Count(); i++)
            {
                if ((i + 1) != ds_id[i])
                {

                    break;
                }
            }
            id = i + 1;
            string s = "";
            if (id < 10)
            {
                s = "HH00" + id.ToString();
            }
            else if (id < 100)
            {
                s = "HH0" + id.ToString();
            }
            else
            {
                s = "HH" + id.ToString();
            }
            return s;
        }
        public void ThemHangHoa(Model.HANGHOA hh)
        {
            dao.ThemHangHoa(hh);
        }
    }
}
