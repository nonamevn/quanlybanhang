﻿using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class PhieuMuaHangDao
    {
        public List<PhieuMuaHang> DsPhieuBanHang()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.PHIEUNHAPHANG.Select(k => new PhieuMuaHang
                {
                    MaPhieuNhapHang = k.MaPhieuNhapHang
                })
                .ToList();
                return list;
            }
        }
        public List<ChiTietPhieuNhapHang> DsChiTiet()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.CHITIETPHIEUNHAPHANG.Select(k => new ChiTietPhieuNhapHang
                {
                    MaChiTietPhieuNhapHang = k.MaChiTietPhieuNhapHang
                })
                .ToList();
                return list;
            }
        }
        public void ThemPhieuMuaHang(PhieuMuaHang p)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                PHIEUNHAPHANG pb = new PHIEUNHAPHANG();
                pb.MaPhieuNhapHang = p.MaPhieuNhapHang;
                pb.MaNhaCungCap = p.MaNhaCungCap;
                pb.DiaChi = p.DiaChi;
                pb.DienThoai = p.DienThoai;
                pb.NgayNhap = p.NgayNhap;
                pb.GhiChu = p.GhiChu;
                pb.NhanVien = p.NhanVien;
                pb.KhoNhap = p.KhoNhap;
                pb.DieuKhoanThanhToan = p.DieuKhoanThanhToan;
                pb.HinhThucThanhToan = p.HinhThucThanhToan;   
                pb.HanThanhToan = p.HanThanhToan;
                ctx.PHIEUNHAPHANG.Add(pb);
                ctx.SaveChanges();
            }
        }
    }
}
