﻿using QLBanHang.Model;
using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class KhachHangDao
    {
        public List<KhachHangModel> DanhSachKH()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.KHACHHANG.Select(k => new KhachHangModel
                {
                    MaKhachHang = k.MaKhachHang,
                    TenKhachHang = k.TenKhachHang
                })
                .ToList();
                return list;
            }
        }
        public KhachHangModel TimTheoMaKH(string MaKH)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var Kh = ctx.KHACHHANG
                    .Where(p => p.MaKhachHang.Trim() == MaKH.Trim())
                    .FirstOrDefault();
                KhachHangModel kq = new KhachHangModel();
                if(Kh != null)
                {
                    kq.TenKhachHang = Kh.TenKhachHang;
                    kq.DiDong = Kh.DiDong;
                    kq.DiaChi = Kh.DiaChi;
                    kq.DienThoai = Kh.DienThoai;
                }
                return kq;
            }
        }
        public void ThemKhachHang(KhachHangModel k)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                KHACHHANG kh = new KHACHHANG();
                kh.MaKhachHang = k.MaKhachHang;
                kh.TenKhachHang = k.TenKhachHang;
                kh.MaKhuVuc = k.MaKhuVuc;
                kh.DiaChi = k.DiaChi;
                kh.MaSoThue = k.MaSoThue;
                kh.DienThoai = k.DienThoai;
                kh.Email = k.Email;
                kh.SoTaiKhoan = k.SoTaiKhoan;
                kh.Fax = k.Fax;
                kh.DiDong = k.DiDong;
                kh.Website = k.Website;
                kh.TenNganHang = k.TenNganHang;
                kh.NickYaHoo = k.NickYaHoo;
                kh.NickSkype = k.NickSkype;
                kh.NguoiLienHe = k.NguoiLienHe;
                kh.ChietKhau = k.ChietKhau;
                kh.GioiHanNo = k.GioiHanNo;
                kh.NoHienTai = 0;
                kh.ConQuanLyKhachHang = k.ConQuanLyKhachHang;
                ctx.KHACHHANG.Add(kh);
                ctx.SaveChanges();
            }
        }
    }
}
