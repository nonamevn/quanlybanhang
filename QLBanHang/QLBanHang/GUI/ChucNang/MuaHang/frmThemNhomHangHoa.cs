﻿using QuanLyBanHang.BUS.ChucNang;
using QuanLyBanHang.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    public partial class frmThemNhomHangHoa : Form
    {
        public Action<String> GetMaNhomHang;
        public frmThemNhomHangHoa()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            if (GetMaNhomHang != null)
            {
                GetMaNhomHang.Invoke(txtMa.Text.Trim());
            }
            Close();
        }
        private bool validate()
        {
            if(string.IsNullOrEmpty(txtMa.Text) || string.IsNullOrWhiteSpace(txtMa.Text.Trim()))
            {
                MessageBox.Show("Mã Nhóm Hàng là bắt buộc");
                return false;
            }
            if (string.IsNullOrEmpty(txtTen.Text) || string.IsNullOrWhiteSpace(txtTen.Text.Trim()))
            {
                MessageBox.Show("Tên Nhóm Hàng là bắt buộc");
                return false;
            }
            if (string.IsNullOrEmpty(txtGhiChu.Text) || string.IsNullOrWhiteSpace(txtGhiChu.Text.Trim()))
            {
                MessageBox.Show("Ghi Chú là bắt buộc");
                return false;
            }
            return true;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            NHOMHANG model = new NHOMHANG();
            model.MaNhomHang = txtMa.Text.Trim();
            model.TenNhomHang = txtTen.Text.Trim();
            model.GhiChu = txtGhiChu.Text.Trim();
            model.ConQuanLyDonNhomHang = chkConQuanLy.Checked;
            if(validate())
            {
                try
                {
                    if(NhomHangBus.AddNhomHang(model))
                    {
                        MessageBox.Show("Thêm thành công");
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
