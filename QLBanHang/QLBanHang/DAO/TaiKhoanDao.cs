﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLBanHang.Model;
using QuanLyBanHang.Model;
using QuanLyBanHang.Model.DanhMuc;
using QuanLyBanHang.Model.Sesstion;

namespace QuanLyBanHang.DAO.ChucNang
{
    class TaiKhoanDao
    {
        public List<TaiKhoanModel> DsTaiKhoan()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.TAIKHOAN.Select(k =>
                   new TaiKhoanModel
                   {
                       TenDangNhap = k.TenDangNhap,
                       MatKhau = k.MatKhau
                   }
                    ).ToList();
                return list;
            }
        }
       
        public static Boolean CheckLogin(string taiKhoan, string matKhau)
        {
            try
            {
                using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
                {
                    //Tim Tai Khoan

                    var TK = ctx.TAIKHOAN.Where(p => p.TenDangNhap == taiKhoan).FirstOrDefault();
                    if (TK != null)
                    {
                        if (TK.MatKhau == matKhau)
                        {
                            Session_NguoiDung_Model.TenDangNhap=TK.TenDangNhap;
                            return true;
                        }
                        else
                        {
                            //Sai pas
                            return false;
                        }
                    }
                    else
                    {
                        //Khong tin thay
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
                
        }
        public string LayMatKhau(string taiKhoan)
        {
            using (var ctx = new QuanLyBanHangEntities())
            {
                var TK = ctx.TAIKHOAN
                    .Where(p => p.TenDangNhap.Trim() == taiKhoan.Trim())
                    .FirstOrDefault();
                if (TK != null)
                {
                    return TK.MatKhau;
                }
                else
                    return null;
            }
        }
        public void DoiMatKhau(string taiKhoan,string matKhauMoi)
        {
            using (var ctx = new QuanLyBanHangEntities())
            {
                var TK = ctx.TAIKHOAN
                    .Where(p => p.TenDangNhap.Trim() == taiKhoan.Trim()).FirstOrDefault();
                if(TK!=null)
                {
                    TK.MatKhau = matKhauMoi;
                    ctx.SaveChanges();
                }
                else
                {
                   
                }
            }
        }
        public string LayVaiTro(string taiKhoan)
        {
            using (var ctx = new QuanLyBanHangEntities())
            {
                var TK = ctx.TAIKHOAN
                    .Where(p => p.TenDangNhap.Trim() == taiKhoan.Trim()).FirstOrDefault();
                if(TK!=null)
                {
                    return TK.VAITRO.ToString();
                }
                else
                {
                    return null;
                }
            }
        }

       
    }
}
