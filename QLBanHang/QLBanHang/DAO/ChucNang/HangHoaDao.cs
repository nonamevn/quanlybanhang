﻿
using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class HangHoaDao
    {
        public List<HangHoaModel> DsHangHoa()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.HANGHOA.Select( k=>
                    new HangHoaModel {MaHangHoa = k.MaHangHoa,
                    TenHangHoa = k.TenHangHoa,
                    TenDonVi = k.DONVITINH.TenDonVi,
                    TonKhoHienTai = k.TonKhoHienTai,
                    TenKho = k.KHOHANG.TenKho,
                    GiaMua = k.GiaMua,
                    GiaBanLe = k.GiaBanLe,
                    GiaBanSi = k.GiaBanSi}
                    ).ToList();
                return list;   
            }
        }
        public string TimMa(string Ten)
        {
            using (var ctx = new QuanLyBanHangEntities())
            {
                var Ma = ctx.HANGHOA
                    .Where(p => p.TenHangHoa.Contains(Ten))
                    .FirstOrDefault();
                if(Ma != null)
                {
                    return Ma.MaHangHoa;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public HangHoaModel TimTheoMaHH(string MaHH)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var Hh = ctx.HANGHOA
                    .Where(p => p.MaHangHoa.Trim() == MaHH.Trim())
                    .FirstOrDefault();
                HangHoaModel kq = new HangHoaModel();
                if (Hh != null)
                {
                    kq.MaHangHoa = Hh.MaHangHoa;
                    kq.TenHangHoa = Hh.TenHangHoa;
                    kq.TenDonVi = Hh.DONVITINH.TenDonVi;
                    kq.TonKhoHienTai = Hh.TonKhoHienTai;
                   
                    kq.GiaMua = Hh.GiaMua;
                    kq.GiaBanLe = Hh.GiaBanLe;
                    kq.GiaBanSi = Hh.GiaBanSi;
                }
                return kq;
            }
        }
        public void ThemHangHoa(HANGHOA hh)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                ctx.HANGHOA.Add(hh);
                ctx.SaveChanges();
            }
        }
    }
}
