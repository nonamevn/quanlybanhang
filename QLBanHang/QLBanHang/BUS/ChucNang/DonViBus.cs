﻿using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class DonViBus
    {
        DonViDao dao = new DonViDao();
        public List<DonViModel> DsDonVi()
        {
            return dao.DsDonVi();
        }
        public static bool AddDonViTinh(DONVITINH model)
        {
            try
            {
                return DonViDao.AddDonViTinh(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
