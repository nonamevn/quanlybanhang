﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.ChucNang
{
    public class ChiTietPhieuBanHang
    {
        public string MaChiTietPhieuBanHang { get; set; }
        public string MaPhieuBanHang { get; set; }
        public string MaHangHoa { get; set; }
        public int SoLuong { get; set; }
        public decimal DonGia { get; set;}
        public string ThanhTien { get; set; }
        public string ChietKhau { get; set; }
        public string DonViTinh { get; set; }
        public string _ChietKhau { get; set; }
        public decimal ThanhToan { get; set; }
        public string TenHangHoa { get; set; }
        public string DonVi { get; set; }
        public decimal Gia { get; set; }
    }
}
