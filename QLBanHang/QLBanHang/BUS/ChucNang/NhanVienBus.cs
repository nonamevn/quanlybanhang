﻿using QLBanHang.Model;
using QuanLyBanHang.DAO.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class NhanVienBus
    {
        NhanVienDao dao = new NhanVienDao();
        public List<NhanVienModel> DsNhanVien()
        {
            return dao.DsNhanVien();
        }
    }
}
