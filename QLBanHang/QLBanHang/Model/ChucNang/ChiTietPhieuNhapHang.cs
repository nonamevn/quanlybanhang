﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.ChucNang
{
    class ChiTietPhieuNhapHang
    {
        public string MaChiTietPhieuNhapHang { get; set; }
        public string MaPhieuNhapHang { get; set; }
        public string MaHangHoa { get; set; }
        public int SoLuong { get; set; }
        public decimal ThanhTien { get; set; }
        public string GhiChu { get; set; }
        public string TenDonVi { get; set; }
        public decimal DonGia { get; set; }
        public string TenHangHoa { get; set; }
        
    }
}
