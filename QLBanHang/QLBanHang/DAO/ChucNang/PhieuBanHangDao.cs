﻿using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class PhieuBanHangDao
    {
        public List<PhieuBanHang> DsPhieuBanHang()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.PHIEUBANHANG.Select(k => new PhieuBanHang
                {
                    MaPhieuBanHang = k.MaPhieuBanHang
                })
                .ToList();
                return list;
            }
        }
        public List<ChiTietPhieuBanHang> DsChiTiet()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.CHITIETPHIEUBANHANG.Select(k => new ChiTietPhieuBanHang
                {
                    MaChiTietPhieuBanHang = k.MaChiTietPhieuBanHang
                })
                .ToList();
                return list;
            }
        }
        public void ThemPhieuBanHang(PhieuBanHang p)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                PHIEUBANHANG pb = new PHIEUBANHANG();
                pb.MaPhieuBanHang = p.MaPhieuBanHang;
                pb.MaKhachHang = p.MaKhachHang;
                pb.DiaChi = p.DiaChi;
                pb.DienThoai = p.DienThoai;
                pb.NgayBan = p.NgayBan;
                pb.GhiChu = p.GhiChu;
                pb.NhanVien = p.NhanVien;
                pb.KhoXuat = p.KhoXuat;
                pb.DieuKhoanThanhToan = p.DieuKhoanThanhToan;
                pb.HinhThucThanhToan = p.HinhThucThanhToan;
                pb.ThanhTien = p.ThanhTien;
                pb.HanThanhToan = p.HanThanhToan;
                pb.NgayGiao = p.NgayGiao;
                ctx.PHIEUBANHANG.Add(pb);
                ctx.SaveChanges();
            }
        }
    }
}
