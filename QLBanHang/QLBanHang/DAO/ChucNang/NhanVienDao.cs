﻿using QLBanHang.Model;
using QuanLyBanHang.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class NhanVienDao
    {
        public List<NhanVienModel> DsNhanVien()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.NHANVIEN.Select(k =>
                   new NhanVienModel
                   {
                       MaNhanVien = k.MaNhanVien,
                       HoTen = k.HoTen
                   }
                    ).ToList();
                return list;
            }
        }
    }
}
