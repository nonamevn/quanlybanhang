﻿using QuanLyBanHang.DAO.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyBanHang.Model.ChucNang;

namespace QuanLyBanHang.BUS.ChucNang
{
    class PhieuBanHangBus
    {
        PhieuBanHangDao dao = new PhieuBanHangDao();
        public string MaPhieuBanHangMoi()
        {
            var list = dao.DsPhieuBanHang();
            List<string> ds = new List<string>();
            foreach (var item in list)
            {
                ds.Add(item.MaPhieuBanHang);
            }
            List<int> ds_id = new List<int>();
            foreach (var rs in ds)
            {
                rs.Trim();
                ds_id.Add(Int32.Parse(rs.Substring(2)));
            }
            ds_id.Sort();
            int id = ds_id.Count();
            int i;
            for (i = 0; i < ds_id.Count(); i++)
            {
                if ((i + 1) != ds_id[i])
                {

                    break;
                }
            }
            id = i + 1;
            string s = "";
            if (id < 10)
            {
                s = "BH00" + id.ToString();
            }
            else if (id < 100)
            {
                s = "BH0" + id.ToString();
            }
            else
            {
                s = "BH" + id.ToString();
            }
            return s;
        }
        public string MaChiTietMoi()
        {
            var list = dao.DsChiTiet();
            List<string> ds = new List<string>();
            foreach (var item in list)
            {
                ds.Add(item.MaChiTietPhieuBanHang);
            }
            List<int> ds_id = new List<int>();
            foreach (var rs in ds)
            {
                rs.Trim();
                ds_id.Add(Int32.Parse(rs.Substring(2)));
            }
            ds_id.Sort();
            int id = ds_id.Count();
            int i;
            for (i = 0; i < ds_id.Count(); i++)
            {
                if ((i + 1) != ds_id[i])
                {

                    break;
                }
            }
            id = i + 1;
            string s = "";
            if (id < 10)
            {
                s = "CT00" + id.ToString();
            }
            else if (id < 100)
            {
                s = "CT0" + id.ToString();
            }
            else
            {
                s = "CT" + id.ToString();
            }
            return s;
        }
        public void ThemPhieuBanHang(PhieuBanHang pb)
        {
            dao.ThemPhieuBanHang(pb);
        }
    }
}
