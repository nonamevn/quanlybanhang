﻿using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class NhomHangDao
    {
        public List<NhomHang> DsNhomHang()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.NHOMHANG.Select(k => new NhomHang
                {
                    MaNhomHang = k.MaNhomHang,
                    TenNhomHang = k.TenNhomHang
                })
                .ToList();
                return list;
            }
        }
        public static bool AddNhomHang(NHOMHANG model)
        {
            try
            {
                using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
                {
                    ctx.NHOMHANG.Add(model);
                    ctx.SaveChanges();
                    return true;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
