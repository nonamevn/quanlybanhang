﻿namespace QLBanHang
{
    partial class frmHangHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHangHoa));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnThemDonVi = new DevExpress.XtraEditors.SimpleButton();
            this.lkDonVi = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnThemPhanLoai = new DevExpress.XtraEditors.SimpleButton();
            this.lkPhanLoai = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtMaVach = new DevExpress.XtraEditors.TextEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.txtXuatXu = new DevExpress.XtraEditors.TextEdit();
            this.txtTenHang = new DevExpress.XtraEditors.TextEdit();
            this.txtMHang = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.cbTonKhoToiThieu = new DevExpress.XtraEditors.CalcEdit();
            this.cbTonHienTai = new DevExpress.XtraEditors.CalcEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.btnThemNhaCungCap = new DevExpress.XtraEditors.SimpleButton();
            this.lkNhaCungCap = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.chkConQuanLy = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.cbGiaMua = new DevExpress.XtraEditors.CalcEdit();
            this.cbGiaBanLe = new DevExpress.XtraEditors.CalcEdit();
            this.cbGiaBanSi = new DevExpress.XtraEditors.CalcEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.btnLSGD = new DevExpress.XtraEditors.SimpleButton();
            this.lkKho = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnThemKhuVuc = new DevExpress.XtraEditors.SimpleButton();
            this.cbLoaiHang = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkDonVi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkPhanLoai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaVach.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXuatXu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMHang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTonKhoToiThieu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTonHienTai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkNhaCungCap.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConQuanLy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaMua.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaBanLe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaBanSi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkKho.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(25, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(69, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Loại Hàng Hóa";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(353, 9);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(65, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Kho Mặc Định";
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.btnThemDonVi);
            this.groupControl1.Controls.Add(this.lkDonVi);
            this.groupControl1.Controls.Add(this.btnThemPhanLoai);
            this.groupControl1.Controls.Add(this.lkPhanLoai);
            this.groupControl1.Controls.Add(this.txtMaVach);
            this.groupControl1.Controls.Add(this.labelControl16);
            this.groupControl1.Controls.Add(this.labelControl18);
            this.groupControl1.Controls.Add(this.labelControl19);
            this.groupControl1.Controls.Add(this.labelControl17);
            this.groupControl1.Controls.Add(this.txtXuatXu);
            this.groupControl1.Controls.Add(this.txtTenHang);
            this.groupControl1.Controls.Add(this.txtMHang);
            this.groupControl1.Controls.Add(this.labelControl10);
            this.groupControl1.Controls.Add(this.labelControl9);
            this.groupControl1.Controls.Add(this.labelControl8);
            this.groupControl1.Controls.Add(this.labelControl7);
            this.groupControl1.Controls.Add(this.labelControl6);
            this.groupControl1.Controls.Add(this.labelControl5);
            this.groupControl1.Controls.Add(this.labelControl4);
            this.groupControl1.Controls.Add(this.labelControl3);
            this.groupControl1.Controls.Add(this.textEdit1);
            this.groupControl1.Controls.Add(this.cbTonKhoToiThieu);
            this.groupControl1.Controls.Add(this.cbTonHienTai);
            this.groupControl1.Location = new System.Drawing.Point(4, 38);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(622, 264);
            this.groupControl1.TabIndex = 2;
            this.groupControl1.Text = "Thông Tin Chung";
            // 
            // btnThemDonVi
            // 
            this.btnThemDonVi.Image = ((System.Drawing.Image)(resources.GetObject("btnThemDonVi.Image")));
            this.btnThemDonVi.Location = new System.Drawing.Point(286, 133);
            this.btnThemDonVi.Name = "btnThemDonVi";
            this.btnThemDonVi.Size = new System.Drawing.Size(24, 19);
            this.btnThemDonVi.TabIndex = 70;
            this.btnThemDonVi.Click += new System.EventHandler(this.btnThemDonVi_Click);
            // 
            // lkDonVi
            // 
            this.lkDonVi.EditValue = "[Chon Khu Vuc]";
            this.lkDonVi.Location = new System.Drawing.Point(161, 132);
            this.lkDonVi.Name = "lkDonVi";
            this.lkDonVi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkDonVi.Properties.View = this.gridView7;
            this.lkDonVi.Size = new System.Drawing.Size(163, 20);
            this.lkDonVi.TabIndex = 69;
            // 
            // gridView7
            // 
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            // 
            // btnThemPhanLoai
            // 
            this.btnThemPhanLoai.Image = ((System.Drawing.Image)(resources.GetObject("btnThemPhanLoai.Image")));
            this.btnThemPhanLoai.Location = new System.Drawing.Point(572, 29);
            this.btnThemPhanLoai.Name = "btnThemPhanLoai";
            this.btnThemPhanLoai.Size = new System.Drawing.Size(24, 19);
            this.btnThemPhanLoai.TabIndex = 69;
            this.btnThemPhanLoai.Click += new System.EventHandler(this.btnThemPhanLoai_Click);
            // 
            // lkPhanLoai
            // 
            this.lkPhanLoai.EditValue = "[Chon Khu Vuc]";
            this.lkPhanLoai.Location = new System.Drawing.Point(161, 28);
            this.lkPhanLoai.Name = "lkPhanLoai";
            this.lkPhanLoai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkPhanLoai.Properties.View = this.gridView5;
            this.lkPhanLoai.Size = new System.Drawing.Size(452, 20);
            this.lkPhanLoai.TabIndex = 69;
            // 
            // gridView5
            // 
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // txtMaVach
            // 
            this.txtMaVach.Location = new System.Drawing.Point(450, 57);
            this.txtMaVach.Name = "txtMaVach";
            this.txtMaVach.Size = new System.Drawing.Size(163, 20);
            this.txtMaVach.TabIndex = 15;
            // 
            // labelControl16
            // 
            this.labelControl16.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl16.Location = new System.Drawing.Point(99, 134);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(14, 13);
            this.labelControl16.TabIndex = 19;
            this.labelControl16.Text = "(*)";
            // 
            // labelControl18
            // 
            this.labelControl18.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl18.Location = new System.Drawing.Point(99, 98);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(14, 13);
            this.labelControl18.TabIndex = 21;
            this.labelControl18.Text = "(*)";
            // 
            // labelControl19
            // 
            this.labelControl19.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl19.Location = new System.Drawing.Point(99, 33);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(14, 13);
            this.labelControl19.TabIndex = 22;
            this.labelControl19.Text = "(*)";
            // 
            // labelControl17
            // 
            this.labelControl17.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl17.Location = new System.Drawing.Point(99, 64);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(14, 13);
            this.labelControl17.TabIndex = 20;
            this.labelControl17.Text = "(*)";
            // 
            // txtXuatXu
            // 
            this.txtXuatXu.Location = new System.Drawing.Point(161, 161);
            this.txtXuatXu.Name = "txtXuatXu";
            this.txtXuatXu.Size = new System.Drawing.Size(163, 20);
            this.txtXuatXu.TabIndex = 13;
            // 
            // txtTenHang
            // 
            this.txtTenHang.Location = new System.Drawing.Point(161, 91);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(452, 20);
            this.txtTenHang.TabIndex = 11;
            // 
            // txtMHang
            // 
            this.txtMHang.Location = new System.Drawing.Point(161, 57);
            this.txtMHang.Name = "txtMHang";
            this.txtMHang.Size = new System.Drawing.Size(163, 20);
            this.txtMHang.TabIndex = 10;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(42, 134);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(31, 13);
            this.labelControl10.TabIndex = 7;
            this.labelControl10.Text = "Đơn Vị";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(365, 60);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(62, 13);
            this.labelControl9.TabIndex = 6;
            this.labelControl9.Text = "Mã Vạch NSX";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(43, 235);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(59, 13);
            this.labelControl8.TabIndex = 5;
            this.labelControl8.Text = "Tồn Hiện Tại";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(43, 202);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(85, 13);
            this.labelControl7.TabIndex = 4;
            this.labelControl7.Text = "Tồn Kho Tối Thiểu";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(42, 168);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(38, 13);
            this.labelControl6.TabIndex = 3;
            this.labelControl6.Text = "Xuất Xứ";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(42, 98);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(46, 13);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "Tên Hàng";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(43, 64);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(42, 13);
            this.labelControl4.TabIndex = 1;
            this.labelControl4.Text = "Mã Hàng";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(43, 33);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(46, 13);
            this.labelControl3.TabIndex = 0;
            this.labelControl3.Text = "Phân Loại";
            // 
            // textEdit1
            // 
            this.textEdit1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit1.EditValue = "\t\t\t\t\r\n\r\n\r\n\r\n\t\t        Ảnh";
            this.textEdit1.Location = new System.Drawing.Point(349, 127);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.textEdit1.Size = new System.Drawing.Size(264, 121);
            this.textEdit1.TabIndex = 29;
            // 
            // cbTonKhoToiThieu
            // 
            this.cbTonKhoToiThieu.Location = new System.Drawing.Point(161, 194);
            this.cbTonKhoToiThieu.Name = "cbTonKhoToiThieu";
            this.cbTonKhoToiThieu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTonKhoToiThieu.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.cbTonKhoToiThieu.Properties.NullText = "[EditValue is null]";
            this.cbTonKhoToiThieu.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTonKhoToiThieu.Size = new System.Drawing.Size(163, 20);
            this.cbTonKhoToiThieu.TabIndex = 27;
            // 
            // cbTonHienTai
            // 
            this.cbTonHienTai.Location = new System.Drawing.Point(161, 228);
            this.cbTonHienTai.Name = "cbTonHienTai";
            this.cbTonHienTai.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbTonHienTai.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.cbTonHienTai.Properties.NullText = "[EditValue is null]";
            this.cbTonHienTai.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbTonHienTai.Size = new System.Drawing.Size(163, 20);
            this.cbTonHienTai.TabIndex = 28;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.btnThemNhaCungCap);
            this.groupControl2.Controls.Add(this.lkNhaCungCap);
            this.groupControl2.Controls.Add(this.chkConQuanLy);
            this.groupControl2.Controls.Add(this.labelControl11);
            this.groupControl2.Controls.Add(this.labelControl12);
            this.groupControl2.Controls.Add(this.labelControl13);
            this.groupControl2.Controls.Add(this.labelControl14);
            this.groupControl2.Controls.Add(this.cbGiaMua);
            this.groupControl2.Controls.Add(this.cbGiaBanLe);
            this.groupControl2.Controls.Add(this.cbGiaBanSi);
            this.groupControl2.Location = new System.Drawing.Point(4, 308);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(622, 122);
            this.groupControl2.TabIndex = 3;
            this.groupControl2.Text = "Thông Tin Giao Dịch";
            // 
            // btnThemNhaCungCap
            // 
            this.btnThemNhaCungCap.Image = ((System.Drawing.Image)(resources.GetObject("btnThemNhaCungCap.Image")));
            this.btnThemNhaCungCap.Location = new System.Drawing.Point(572, 26);
            this.btnThemNhaCungCap.Name = "btnThemNhaCungCap";
            this.btnThemNhaCungCap.Size = new System.Drawing.Size(24, 19);
            this.btnThemNhaCungCap.TabIndex = 71;
            this.btnThemNhaCungCap.Click += new System.EventHandler(this.btnThemNhaCungCap_Click);
            // 
            // lkNhaCungCap
            // 
            this.lkNhaCungCap.EditValue = "[Chon Khu Vuc]";
            this.lkNhaCungCap.Location = new System.Drawing.Point(161, 25);
            this.lkNhaCungCap.Name = "lkNhaCungCap";
            this.lkNhaCungCap.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkNhaCungCap.Properties.View = this.gridView8;
            this.lkNhaCungCap.Size = new System.Drawing.Size(452, 20);
            this.lkNhaCungCap.TabIndex = 71;
            // 
            // gridView8
            // 
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // chkConQuanLy
            // 
            this.chkConQuanLy.Location = new System.Drawing.Point(365, 92);
            this.chkConQuanLy.Name = "chkConQuanLy";
            this.chkConQuanLy.Properties.Caption = "Còn Quản Lý";
            this.chkConQuanLy.Size = new System.Drawing.Size(90, 19);
            this.chkConQuanLy.TabIndex = 12;
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(365, 61);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(50, 13);
            this.labelControl11.TabIndex = 11;
            this.labelControl11.Text = "Giá Bán Lẻ";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(43, 95);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(47, 13);
            this.labelControl12.TabIndex = 10;
            this.labelControl12.Text = "Giá Bán Sỉ";
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(44, 61);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(38, 13);
            this.labelControl13.TabIndex = 9;
            this.labelControl13.Text = "Giá Mua";
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(44, 30);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(69, 13);
            this.labelControl14.TabIndex = 8;
            this.labelControl14.Text = "Nhà Cung Cấp";
            // 
            // cbGiaMua
            // 
            this.cbGiaMua.Location = new System.Drawing.Point(161, 58);
            this.cbGiaMua.Name = "cbGiaMua";
            this.cbGiaMua.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGiaMua.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.cbGiaMua.Properties.NullText = "[EditValue is null]";
            this.cbGiaMua.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGiaMua.Size = new System.Drawing.Size(163, 20);
            this.cbGiaMua.TabIndex = 28;
            // 
            // cbGiaBanLe
            // 
            this.cbGiaBanLe.Location = new System.Drawing.Point(450, 58);
            this.cbGiaBanLe.Name = "cbGiaBanLe";
            this.cbGiaBanLe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGiaBanLe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.cbGiaBanLe.Properties.NullText = "[EditValue is null]";
            this.cbGiaBanLe.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGiaBanLe.Size = new System.Drawing.Size(163, 20);
            this.cbGiaBanLe.TabIndex = 30;
            // 
            // cbGiaBanSi
            // 
            this.cbGiaBanSi.Location = new System.Drawing.Point(161, 91);
            this.cbGiaBanSi.Name = "cbGiaBanSi";
            this.cbGiaBanSi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbGiaBanSi.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None;
            this.cbGiaBanSi.Properties.NullText = "[EditValue is null]";
            this.cbGiaBanSi.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbGiaBanSi.Size = new System.Drawing.Size(163, 20);
            this.cbGiaBanSi.TabIndex = 29;
            // 
            // labelControl15
            // 
            this.labelControl15.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl15.Location = new System.Drawing.Point(100, 8);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(14, 13);
            this.labelControl15.TabIndex = 18;
            this.labelControl15.Text = "(*)";
            // 
            // labelControl20
            // 
            this.labelControl20.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl20.Location = new System.Drawing.Point(424, 9);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(14, 13);
            this.labelControl20.TabIndex = 23;
            this.labelControl20.Text = "(*)";
            // 
            // btnDong
            // 
            this.btnDong.Image = ((System.Drawing.Image)(resources.GetObject("btnDong.Image")));
            this.btnDong.Location = new System.Drawing.Point(538, 449);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(88, 23);
            this.btnDong.TabIndex = 17;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.Location = new System.Drawing.Point(443, 449);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(89, 23);
            this.btnLuu.TabIndex = 16;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // btnLSGD
            // 
            this.btnLSGD.Location = new System.Drawing.Point(4, 449);
            this.btnLSGD.Name = "btnLSGD";
            this.btnLSGD.Size = new System.Drawing.Size(136, 23);
            this.btnLSGD.TabIndex = 15;
            this.btnLSGD.Text = "Lịch Sử Giao Dịch";
            // 
            // lkKho
            // 
            this.lkKho.EditValue = "[Chon Khu Vuc]";
            this.lkKho.Location = new System.Drawing.Point(443, 5);
            this.lkKho.Name = "lkKho";
            this.lkKho.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lkKho.Properties.View = this.gridView6;
            this.lkKho.Size = new System.Drawing.Size(183, 20);
            this.lkKho.TabIndex = 57;
            // 
            // gridView6
            // 
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // btnThemKhuVuc
            // 
            this.btnThemKhuVuc.Image = ((System.Drawing.Image)(resources.GetObject("btnThemKhuVuc.Image")));
            this.btnThemKhuVuc.Location = new System.Drawing.Point(584, 6);
            this.btnThemKhuVuc.Name = "btnThemKhuVuc";
            this.btnThemKhuVuc.Size = new System.Drawing.Size(24, 19);
            this.btnThemKhuVuc.TabIndex = 68;
            this.btnThemKhuVuc.Click += new System.EventHandler(this.btnThemKhuVuc_Click);
            // 
            // cbLoaiHang
            // 
            this.cbLoaiHang.FormattingEnabled = true;
            this.cbLoaiHang.Location = new System.Drawing.Point(165, 4);
            this.cbLoaiHang.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbLoaiHang.Name = "cbLoaiHang";
            this.cbLoaiHang.Size = new System.Drawing.Size(165, 21);
            this.cbLoaiHang.TabIndex = 69;
            // 
            // frmHangHoa
            // 
            this.AcceptButton = this.btnLuu;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(631, 486);
            this.Controls.Add(this.cbLoaiHang);
            this.Controls.Add(this.btnThemKhuVuc);
            this.Controls.Add(this.lkKho);
            this.Controls.Add(this.labelControl20);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.btnDong);
            this.Controls.Add(this.btnLuu);
            this.Controls.Add(this.btnLSGD);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "frmHangHoa";
            this.Text = "Thêm Hàng Hóa, Dịch Vụ";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkDonVi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkPhanLoai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaVach.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXuatXu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMHang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTonKhoToiThieu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbTonHienTai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lkNhaCungCap.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConQuanLy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaMua.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaBanLe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbGiaBanSi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lkKho.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit txtMaVach;
        private DevExpress.XtraEditors.LabelControl labelControl16;
        private DevExpress.XtraEditors.LabelControl labelControl18;
        private DevExpress.XtraEditors.LabelControl labelControl19;
        private DevExpress.XtraEditors.LabelControl labelControl17;
        private DevExpress.XtraEditors.TextEdit txtXuatXu;
        private DevExpress.XtraEditors.TextEdit txtTenHang;
        private DevExpress.XtraEditors.TextEdit txtMHang;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.CheckEdit chkConQuanLy;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.LabelControl labelControl13;
        private DevExpress.XtraEditors.LabelControl labelControl14;
        private DevExpress.XtraEditors.SimpleButton btnLSGD;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraEditors.LabelControl labelControl15;
        private DevExpress.XtraEditors.LabelControl labelControl20;
        private DevExpress.XtraEditors.MemoEdit textEdit1;
        private DevExpress.XtraEditors.GridLookUpEdit lkKho;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.SimpleButton btnThemDonVi;
        private DevExpress.XtraEditors.GridLookUpEdit lkDonVi;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.SimpleButton btnThemPhanLoai;
        private DevExpress.XtraEditors.GridLookUpEdit lkPhanLoai;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.SimpleButton btnThemNhaCungCap;
        private DevExpress.XtraEditors.GridLookUpEdit lkNhaCungCap;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.SimpleButton btnThemKhuVuc;
        private System.Windows.Forms.ComboBox cbLoaiHang;
        private DevExpress.XtraEditors.CalcEdit cbGiaMua;
        private DevExpress.XtraEditors.CalcEdit cbGiaBanLe;
        private DevExpress.XtraEditors.CalcEdit cbGiaBanSi;
        private DevExpress.XtraEditors.CalcEdit cbTonKhoToiThieu;
        private DevExpress.XtraEditors.CalcEdit cbTonHienTai;
    }
}