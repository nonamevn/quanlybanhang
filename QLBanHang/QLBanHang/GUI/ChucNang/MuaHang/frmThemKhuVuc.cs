﻿using QLBanHang.Model;
using QuanLyBanHang.BUS.ChucNang;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLBanHang
{
    
    public partial class frmThemKhuVuc : Form
    {
        public Action<String> GetMaKhuVuc;
        KhuVucBus busKhuVuc = new KhuVucBus();
        public frmThemKhuVuc()
        {
            InitializeComponent();
            Load += FrmThemKhuVuc_Load;
        }
        private void MaKhuVucMoi()
        {
            string id = busKhuVuc.MaKhuVucMoi();
            txtMa.ReadOnly = true;
            txtMa.Text = id;
        }
        private void FrmThemKhuVuc_Load(object sender, EventArgs e)
        {
            MaKhuVucMoi();
            chkConQuanLy.Checked = true;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            Close();
        }
        private bool KiemTra()
        {
            if(txtTen.Text == string.Empty)
            {
                MessageBox.Show("Chưa nhập tên khu vực");
                return false;
            }
            return true;
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(KiemTra() == true)
            {
                KhuVucModel kv = new KhuVucModel();
                kv.MaKhuVuc = txtMa.Text;
                kv.TenKhuVuc = txtTen.Text;
                kv.GhiChu = txtGhiChu.Text;
                if(chkConQuanLy.Checked == true)
                {
                    kv.ConQuanLyKhuVuc = true;
                }
                else
                {
                    kv.ConQuanLyKhuVuc = false;
                }
                busKhuVuc.ThemKhuVuc(kv);
                MessageBox.Show("Thêm thành công", "Thông báo");
                if(GetMaKhuVuc !=null)
                {
                    GetMaKhuVuc.Invoke(txtMa.Text.Trim());
                }
            }
        }
    }
}
