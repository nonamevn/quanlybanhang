﻿using QLBanHang.Model;
using QuanLyBanHang.DAO.ChucNang;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.BUS.ChucNang
{
    class KhachHangBus
    {
        KhachHangDao dao = new KhachHangDao();
        
        public List<KhachHangModel> DanhSachKH()
        {
            return dao.DanhSachKH();
        }
        public KhachHangModel TimTheoMaKH(string MaKH)
        {
            return dao.TimTheoMaKH(MaKH);
        }
        public string MaKhachHangMoi()
        {
            var list = dao.DanhSachKH();
            List<string> ds = new List<string>();
            foreach(var item in list)
            {
                ds.Add(item.MaKhachHang);
            }
            List<int> ds_id = new List<int>();
            foreach (var rs in ds)
            {
                rs.Trim();
                ds_id.Add(Int32.Parse(rs.Substring(2)));
            }
            ds_id.Sort();
            int id = ds_id.Count();
            int i;
            for (i = 0; i < ds_id.Count(); i++)
            {
                if ((i + 1) != ds_id[i])
                {

                    break;
                }
            }
            id = i + 1;
            string s = "";
            if(id < 10)
            {
                s = "KH00" + id.ToString();
            }
            else if(id < 100)
            {
                s = "KH0" + id.ToString();
            }
            else
            {
                s = "KH" + id.ToString();
            }
            return s;
        }
        public void ThemKhachHang(KhachHangModel kh)
        {
            dao.ThemKhachHang(kh);
        }
    }
}
