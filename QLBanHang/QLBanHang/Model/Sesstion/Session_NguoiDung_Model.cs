﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.Sesstion
{
   public static class Session_NguoiDung_Model
    {
        public static string TenNhanVien { get; set; }
        public static string MaNhanVien { get; set; }
        public static string TenDangNhap { get; set; }
    }
}
