﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.Model.ChucNang
{
    class HangHoaModel
    {

        public string MaHangHoa { get; set; }
        public string TenHangHoa { get; set; }
        public string TenKho { get; set; }
        public string TenDonVi { get; set; }
        public decimal GiaMua { get; set; }
        public decimal GiaBanLe { get; set; }
        public decimal GiaBanSi { get; set; }
        public int TonKhoHienTai { get; set; }
    }
}
