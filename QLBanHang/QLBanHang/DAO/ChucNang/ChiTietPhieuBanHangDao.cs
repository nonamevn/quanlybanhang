﻿using QuanLyBanHang.Model;
using QuanLyBanHang.Model.ChucNang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class ChiTietPhieuBanHangDao
    {
        public void ThemChiTiet(ChiTietPhieuBanHang c)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                CHITIETPHIEUBANHANG ct = new CHITIETPHIEUBANHANG();
                ct.MaPhieuBanHang = c.MaPhieuBanHang;
                ct.MaChiTietPhieuBanHang = c.MaChiTietPhieuBanHang;
                ct.MaHangHoa = c.MaHangHoa;
                ct.SoLuong = c.SoLuong;
                ct.DonGia = c.DonGia;
                ct.ThanhTien = decimal.Parse(c.ThanhTien);
                ct.ChietKhau = Int32.Parse(c.ChietKhau);
                ct.ThanhToan = decimal.Parse(c.ThanhToan.ToString());
                ctx.CHITIETPHIEUBANHANG.Add(ct);
                ctx.SaveChanges();
            }
        }
    }
}
