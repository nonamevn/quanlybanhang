﻿using QLBanHang.Model;
using QuanLyBanHang.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyBanHang.DAO.ChucNang
{
    class KhuVucDao
    {
        public List<KhuVucModel> DsKhuVuc()
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                var list = ctx.KHUVUC.Select(p => new KhuVucModel
                {
                   MaKhuVuc = p.MaKhuVuc,
                   TenKhuVuc = p.TenKhuVuc
                }).ToList();
                return list;
            }
        }
        public void ThemKhuVuc(KhuVucModel k)
        {
            using (QuanLyBanHangEntities ctx = new QuanLyBanHangEntities())
            {
                KHUVUC kv = new KHUVUC();
                kv.MaKhuVuc = k.MaKhuVuc;
                kv.TenKhuVuc = k.TenKhuVuc;
                kv.GhiChu = k.GhiChu;
                kv.ConQuanLyKhuVuc = k.ConQuanLyKhuVuc;
                ctx.KHUVUC.Add(kv);
                ctx.SaveChanges();
            }
        }
    }
}
